/**
 * Created by Stefan on 1/24/2016.
 */
var express = require('express');
var apiRouter = express.Router();

var router = function () {
    var repository = require('../repository/stardogDatabaseFacade')();
    var apiService = require('../services/apiService')(repository);
    var apiController = require('../controllers/apiController')(apiService);

    apiRouter.route('/entertainmentItems').get(apiController.getEntertainmentItems);
    apiRouter.route('/test').get(apiController.test);

    apiRouter.route('/users').post(apiController.addUser);
    apiRouter.route('/users/:id').get(apiController.getUser);
    apiRouter.route('/users/:id').put(apiController.updateUser);
    apiRouter.route('/users/:id').delete(apiController.deleteUser);

    return apiRouter;
};

module.exports = router();
