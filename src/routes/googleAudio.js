var express = require('express');
var googleAudioRouter = express.Router();

var router = function () {
    var googleAudio = require('../services/googleAudioService')();
    var authMiddleware = require('../controllers/authenticationMiddleware')();
    var googleAudioController = require('../controllers/googleAudioController')(googleAudio);
    googleAudioRouter.route('/').get(authMiddleware.isAuthorized, googleAudioController.getAudio);

    return googleAudioRouter;
};

module.exports = router();
