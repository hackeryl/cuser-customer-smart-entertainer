/**
 * Created by Stefan on 1/30/2016.
 */
var express = require('express');
var usersRouter = express.Router();

var router = function () {
    var repository = require('../repository/stardogDatabaseFacade')();
    var userFeedbackService = require('../services/userFeedbackService')(repository);
    var userFeedbackController = require('../controllers/userFeedbackController')(userFeedbackService);
    var authMiddleware = require('../controllers/authenticationMiddleware')();

    usersRouter.route('/').post(authMiddleware.isAuthorized, userFeedbackController.addUserFeedback);
    usersRouter.route('/:id').get(authMiddleware.isAuthorized, userFeedbackController.getUserFeedback);
    usersRouter.route('/entertainmentItem/:id').get(authMiddleware.isAuthorized, userFeedbackController.getFeedbackIdsList);

    return usersRouter;
};

module.exports = router();
