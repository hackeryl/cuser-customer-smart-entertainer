/**
 * Created by Vasile on 23-Jan-16.
 */
var express = require('express');
var authRouter = express.Router();

var router = function () {
    var repository = require('../repository/stardogDatabaseFacade')();
    var usersService = require('../services/usersService')(repository);
    var authenticationController = require('../controllers/authenticationController')(usersService);

    authRouter.get('/facebook', authenticationController.facebook);
    authRouter.get('/google', authenticationController.google);
    authRouter.get('/twitter', authenticationController.twitter);
    authRouter.get('/twitter/callback', authenticationController.twitterCallback);
    return authRouter;
};

module.exports = router();
