/**
 * Created by Vasile on 23-Jan-16.
 */
var express = require('express');
var usersRouter = express.Router();

var router = function () {
    var repository = require('../repository/stardogDatabaseFacade')();
    var usersService = require('../services/usersService')(repository);
    var placesService = require('../services/placesService')(repository);
    var productsService = require('../services/productsService')(repository);
    var usersController = require('../controllers/usersController')(usersService, placesService, productsService);

    usersRouter.route('/update').post(usersController.update);
    usersRouter.route('/update/location').post(usersController.updateLocation);
    usersRouter.route('/update/product').post(usersController.updateProduct);

    return usersRouter;
};

module.exports = router();
