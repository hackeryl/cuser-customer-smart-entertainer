/**
 * Created by Vasile on 23-Jan-16.
 */
var express = require('express');
var indexRouter = express.Router();

var router = function () {
    var indexController = require('../controllers/indexController')();
    var authMiddleware = require('../controllers/authenticationMiddleware')();

    indexRouter.route('/login').get(indexController.login);
    indexRouter.route('/locations').get(authMiddleware.isAuthenticated, indexController.locations);
    indexRouter.route('/products').get(authMiddleware.selectedLocation, indexController.products);
    indexRouter.route('/').get(authMiddleware.selectedProduct, indexController.entertainment);
    indexRouter.route('/item/:id').get(authMiddleware.selectedProduct, indexController.entertainmentItem);

    return indexRouter;
};

module.exports = router();
