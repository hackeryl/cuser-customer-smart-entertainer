/**
 * Created by Vasile on 23-Jan-16.
 */
var express = require('express');
var entertainmentRouter = express.Router();

var router = function () {
    var cache = {};
    var repository = require('../repository/stardogDatabaseFacade')();
    var gagService = require('../services/gagService')();
    var googleAudioService = require('../services/googleAudioService')();
    var incdbJokesService = require('../services/incdbJokesService')();
    var youtubeService = require('../services/youtubeService')();
    var randomService = require('../services/randomServiceGenerator');
    var userFeedbackService = require('../services/userFeedbackService')(repository);
    var entertainmentService = require('../services/entertainmentService')(repository, gagService, googleAudioService, incdbJokesService, youtubeService, cache, randomService, userFeedbackService);
    var entertainmentController = require('../controllers/entertainmentController')(entertainmentService);
    var authMiddleware = require('../controllers/authenticationMiddleware')();

    entertainmentRouter.route('/').get(authMiddleware.isAuthorized, entertainmentController.getItems);
    entertainmentRouter.route('/:id').get(authMiddleware.isAuthorized, entertainmentController.getItemById);

    return entertainmentRouter;
};

module.exports = router();
