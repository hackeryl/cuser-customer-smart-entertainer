/**
 * Created by Vasile on 16-Jan-16.
 */
var twitterService = require('../services/twitterService');
var authenticationController = function (usersService) {
    var facebook = function (req, res) {
        res.render('authentication/facebook', {title: 'Login with Facebook'});
    };

    var google = function (req, res) {
        res.render('authentication/google', {title: 'Login with Google'});
    };

    var twitter = function (req, res) {
        var callback = function (err, requestToken, requestSecret) {
            if (err) {
                res.render('error', {
                    title: 'Error - twitter login!',
                    message: err.message,
                    error: err
                });
            }
            else {
                res.redirect("https://api.twitter.com/oauth/authenticate?oauth_token=" + requestToken);
            }
        };

        twitterService.getTwitterRequestToken(callback);
    };

    var twitterCallback = function (req, res) {
        var requestToken = req.query.oauth_token,
            verifier = req.query.oauth_verifier;
        var callback = function (userProfile) {
            if (userProfile == null) {
                res.render('error', {
                    title: 'Error - twitter login!',
                    message: 'Error - twitter login!',
                    error: null
                });
            }
            else {
                usersService.updateUser(userProfile, function (result) {
                    if (result.success !== true) {
                        res.render('error', {
                            title: 'Error - twitter login!',
                            message: err.message,
                            error: err
                        });
                    }
                    else {
                        req.session.user = userProfile;
                        res.redirect('/locations');
                    }
                });
            }
        };
        twitterService.twitterCallback(requestToken, verifier, callback);
    };


    return {
        facebook: facebook,
        google: google,
        twitter: twitter,
        twitterCallback: twitterCallback
    };
};

module.exports = authenticationController;