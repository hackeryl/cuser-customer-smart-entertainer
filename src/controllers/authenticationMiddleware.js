/**
 * Created by Vasile on 28-Jan-16.
 */
var authenticationMiddleware = function () {
    var isAuthorized = function (req, res, next) {
        if (req.session.user && req.session.userLocation && req.session.product) {
            next();
        }
        else {
            res.status(401);
            res.json({success: false, message: "Not authorized to access this resource!"});
        }
    };

    var isAuthenticated = function (req, res, next) {
        if (req.session.user) {
            next();
        }
        else {
            res.redirect("/login");
        }
    };

    var selectedLocation = function (req, res, next) {
        if (req.session.userLocation) {
            next();
        }
        else {
            res.redirect("/locations");
        }
    };

    var selectedProduct = function (req, res, next) {
        if (req.session.product) {
            next();
        }
        else {
            res.redirect("/products");
        }
    };

    return {
        isAuthorized: isAuthorized,
        isAuthenticated: isAuthenticated,
        selectedLocation: selectedLocation,
        selectedProduct: selectedProduct
    };
};

module.exports = authenticationMiddleware;