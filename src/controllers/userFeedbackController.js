/**
 * Created by Stefan on 1/29/2016.
 */
var userFeedbackController = function (userFeedbackService) {
    var addUserFeedback = function (req, res) {

        userFeedbackService.addUserFeedback({
            email: req.session.user.email,
            id: req.body.id,
            rating: req.body.rating,
            review: req.body.review
        }, function(result){
            var response = {};
            response.success = result.success;
            response.message = result.message;
            response.data = result.data;
            res.json(response);
        });
    };

    var getUserFeedback = function (req, res) {
        userFeedbackService.getUserFeedback({
            id: req.params.id
        }, function(result){
            var response = {};
            response.success = result.success;
            response.message = result.message;
            response.data = result.data;
            res.json(response);
        });
    };

    var getFeedbackIdsList = function (req, res) {
        userFeedbackService.getFeedbackIdsList({
            id: req.params.id
        }, function(result){
            var response = {};
            response.success = result.success;
            response.message = result.message;
            response.data = result.data;
            res.json(response);
        });
    };

    return {
        addUserFeedback: addUserFeedback,
        getUserFeedback: getUserFeedback,
        getFeedbackIdsList: getFeedbackIdsList
    };
};

module.exports = userFeedbackController;