/**
 * Created by Stefan on 1/24/2016.
 */
var check = require('check-types');

var apiController = function (apiService) {

    var getEntertainmentItems = function (req, res) {
        apiService.getEntertainmentItems(function (result) {
            res.json(result);
        });
    };


    var test = function (req, res) {
        apiService.test(function (result) {
            res.json(result);
        });
    };

    //  api calls
    var addUser = function (req, res) {


    };

    var getUser = function (req, res) {

        var id = req.params.id;

        if (req.accepts().indexOf("application/rdf+xml") == -1) {

            res.status(406).send();
            return;
        }

        apiService.getUser({id: id}, function (result) {

            if(check.undefined(result)) {

                res.status(404).send();
            } else {

                res.status = 200;
                res.setHeader('content-type', 'application/rdf+xml');
                res.end(result);
            }
        });
    };

    var updateUser = function (req, res) {

        var id = req.params.id;
        var user = req.body.user;

        if(check.undefined(user.email) || check.undefined(user.firstName)) {

            res.status(400).send();
            return;
        }

        user.id = id;
        apiService.updateUser(user, function (result) {

            if(!result.paramsOk) {
                res.status(400).send();
                return;
            }

            if(!result.found) {
                res.status(404).send();
                return;
            }

            if(result.fraud) {
                res.status(403).send();
            } else {
                res.status(200).send();
            }
        });
    };

    var deleteUser = function (req, res) {

    };

    return {
        getEntertainmentItems: getEntertainmentItems,
        test: test,

        //  api calls
        addUser: addUser,
        getUser: getUser,
        updateUser: updateUser,
        deleteUser: deleteUser
    };
};

module.exports = apiController;