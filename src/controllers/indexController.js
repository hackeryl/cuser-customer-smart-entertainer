/**
 * Created by Vasile on 16-Jan-16.
 */
var indexController = function () {
    var login = function (req, res) {
        res.render('login', {title: 'Login'});
    };

    var locations = function (req, res) {
        res.render('locations', {title: 'Select location'});
    };

    var products = function (req, res) {
        res.render('products', {title: 'Select product'});
    };

    var entertainment = function (req, res) {
        res.render('entertainment', {title: 'CuSEr'});
    };

    var entertainmentItem = function (req, res) {
        res.render('entertainmentItem', {title: 'CuSEr', entertainmentItemId: req.params.id});
    };

    return {
        login: login,
        locations: locations,
        products: products,
        entertainment: entertainment,
        entertainmentItem: entertainmentItem
    };
};

module.exports = indexController;