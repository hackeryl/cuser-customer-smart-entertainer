/**
 * Created by Vasile on 16-Jan-16.
 */
var usersController = function (usersService, placesService, productsService) {
    var getUserFromRequest = function (req) {
        var user = JSON.parse(req.body.user);

        return user;
    };

    var getUserLocationFromRequest = function (req) {
        var userLocation = req.body.userLocation;

        return userLocation;
    };

    var update = function (req, res) {
        var user = getUserFromRequest(req);
        usersService.updateUser(user, function (result) {
            req.session.user = user;
            res.json(result);
        });
    };

    var updateLocation = function (req, res) {
        var userLocation = getUserLocationFromRequest(req);
        placesService.savePlace(userLocation, function (result) {
            req.session.userLocation = userLocation;
            res.json(result);
        });
    };

    var updateProduct = function (req, res) {
        productsService.saveProduct(req.body.product, function (result) {
            req.session.product = req.body.product;
            res.json(result);
        });
    };

    return {
        update: update,
        updateLocation: updateLocation,
        updateProduct: updateProduct
    };
};

module.exports = usersController;