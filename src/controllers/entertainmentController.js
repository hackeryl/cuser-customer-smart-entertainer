/**
 * Created by Vasile on 16-Jan-16.
 */
var entertainmentController = function (entertainmentService) {
    var storeItemsInSession = function (session, items) {
        session.items = items;
    };

    var getItems = function (req, res) {
        entertainmentService.getItems(req.session.user, req.session.userLocation, req.session.product, req.query.offset, req.query.limit, function (items) {
            var response = {};
            response.success = true;
            response.message = "CuSEr - Entertainment Items";
            response.data = items;
            storeItemsInSession(req.session, items);
            res.json(response);
        });
    };

    var getItemById = function (req, res) {
        entertainmentService.getItemById(req.params.id, function (item) {
            var response = {};
            if (item) {
                response.success = true;
                response.message = "CuSEr - Entertainment Item";
                response.data = item;
            }
            else {
                response.success = false;
                response.message = "Invalid Entertainment Item Id!";
            }

            res.json(response);
        });
    };

    return {
        getItems: getItems,
        getItemById: getItemById
    };
};

module.exports = entertainmentController;
