var http = require('https');
var googleAudioController = function (googleAudio) {
    var getAudio = function (req, res) {
        googleAudio.getAudioStream( req.query.q, function (url) {
            res.setHeader('content-type', 'audio/mpeg');
            http.get(url, function (ress) {
                res.setHeader('Content-Length', ress.headers['content-length']);
                ress.pipe(res);
                ress.on('end', function () {
                    res.send();
                });
            });
        });

    };

    return {
        getAudio: getAudio
    };
};

module.exports = googleAudioController;