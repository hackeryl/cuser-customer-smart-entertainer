var config = require('../config');

var googleAudionService = function () {
    var getAudioItem = function (text, callback) {
        var audio = {};
        audio.type = config.cuserClasses.audioItem;
        audio.source = config.cuserClasses.googleAudioAPI;
        audio.url = config.googleAudio.url + text;
        audio.title = text;
        audio.src = '/audio?q=' + text;
        setTimeout(function () {
            callback(audio);
        }, 0);
    };

    var getAudioStream = function (text, callback) {
        var url = config.googleAudio.url + text;
        callback(url);
    };

    return {
        getAudioItem: getAudioItem,
        getAudioStream: getAudioStream
    }
};

module.exports = googleAudionService;
