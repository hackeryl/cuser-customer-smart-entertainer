var TwitterApi = require('node-twitter-api');
var config = require('../config');

var twitterService = function () {
    var twitterObject = new TwitterApi({
        consumerKey: config.twitter.consumerKey,
        consumerSecret: config.twitter.consumerSecret,
        callback: config.twitter.callback
    });
    var _requestSecret;
    var userProfile = {};
    var getTwitterRequestToken = function (callback) {
        twitterObject.getRequestToken(function (err, requestToken, requestSecret) {
            _requestSecret = requestSecret;
            callback(err, requestToken, requestSecret);
        });
    };

    var twitterCallback = function (requestToken, verifier, callback) {
        twitterObject.getAccessToken(requestToken, _requestSecret, verifier, function (err, accessToken, accessSecret) {
            if (err) {
                callback(null);
            }
            else {

                twitterObject.verifyCredentials(accessToken, accessSecret, function (err, user) {
                    if (err) {
                        callback(null);
                    }
                    else {
                        user.accessToken = accessToken;
                        user.accessSecret = accessSecret;
                        userProfile.socialPlatform = 'Twitter';
                        userProfile.userId = user.id;
                        userProfile.email = user.screen_name + '@twitter.com';
                        userProfile.username = user.screen_name;
                        userProfile.imageUrl = user.profile_image_url;
                        userProfile.token = null;
                        twitterObject.favorites(
                            "list", {
                                count: "2",
                                screen_name: user.screen_name,
                            },
                            user.accessToken,
                            user.accessSecret,
                            function (error, data, response) {
                                if (error) {
                                    callback(null);
                                }
                                else {
                                    var likes = [];
                                    for (i = 0; i < data.length; i++) {
                                        likes.push(data[i].user.screen_name);
                                    }
                                    userProfile.likes = likes;
                                    twitterObject.friends("list",
                                        {
                                            cursor: "-1",
                                            screen_name: user.screen_name,
                                            skip_status: "true",
                                            include_user_entities: "false"
                                        },
                                        user.accessToken,
                                        user.accessSecret,
                                        function (error, data, response) {
                                            if (error) {
                                                callback(null);
                                            }
                                            else {
                                                var friends = [];
                                                for (i = 0; i < data.users.length; i++) {
                                                    var user = {};
                                                    user.socialPlatform = 'Twitter';
                                                    user.userId = data.users[i].id;
                                                    user.username = data.users[i].screen_name;
                                                    user.email = data.users[i].screen_name + '@twitter.com';
                                                    user.imageUrl = data.users[i].profile_image_url;
                                                    friends.push(user);
                                                }
                                                userProfile.friends = friends;
                                                callback(userProfile);
                                            }
                                        }
                                    );
                                }
                            }
                        );

                    }
                });
            }
        });
    };


    return {
        getTwitterRequestToken: getTwitterRequestToken,
        twitterCallback: twitterCallback
    };
};

module.exports = twitterService();