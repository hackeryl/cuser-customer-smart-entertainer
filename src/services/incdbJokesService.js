
var http = require('http');
var request = require('request');
var config = require('../config');
var icndbJokesService = function () {
    var getJokes = function (count, callback) {
        var url = config.icndb.url + count;
        request(url, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var jokes = JSON.parse(body);
                createList(jokes, function (jokeList) {
                    callback(jokeList);
                });

            }
            else {
                callback([]);
            }
        });

    };

    var getJokesByName = function (count, firstName, lastName, callback) {
        var url = config.icndb.url + count + '?firstName=' + firstName + '&lastName=' + lastName;
        request(url, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var jokes = JSON.parse(body);
                createList(jokes, function (jokeList) {
                    callback(jokeList);
                });
            }
            else {
                callback([]);
            }
        });
    };

    function createList(jokes, callback) {
        var jokesList = [];
        for (var i = 0; i < jokes.value.length; i++) {
            var joke = {};
            joke.type = config.cuserClasses.jokeItem;
            joke.source = config.cuserClasses.icndbAPI;
            joke.joke = jokes.value[i].joke;
            joke.url = config.icndb.src + jokes.value[i].id;
            jokesList.push(joke);
        }
        callback(jokesList);
    };

    return {
        getJokes: getJokes,
        getJokesByName: getJokesByName
    }
};
module.exports = icndbJokesService;