var http = require('http');
var request = require('request');
var options = {
    url: 'https://api.imgur.com/3/gallery.json',
    authorization: 'Client-ID d69ac746450c90a'
}
var imgurService = function () {
    var getImages = function (callback) {

      request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var images = JSON.parse(body);
                var imageList = [];
                for (var i = 0; i < images.data.length; i++) {
                    imageList.push(images.data[i]);
                }
                if (imageList.length == images.data.length) {
                    callback(imageList);
                }
            }
            else {
                callback([]);
            }
        });

    };
    return {
        getImages: getImages
    }
};
module.exports = imgurService;
