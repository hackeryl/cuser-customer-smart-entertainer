/**
 * Created by Vasile on 23-Jan-16.
 */
var config = require('../config');

var entertainmentService = function (repository, gagService, googleAudioService, incdbJokesService, youtubeService, cache, randomService, userFeedbackService) {
    var createFakeEntertainmentItems = function () {
        var items = [];

        var joke = {};
        joke.id = 1;
        joke.type = "cuser:JokeItem";
        joke.source = "ICNDb - The Internet Chuck Norris Database";
        joke.joke = "Chuck Norris will never have a heart attack. His heart isn't nearly foolish enough to attack him.";
        joke.url = "http://api.icndb.com/jokes/395";
        joke.reviewsCount = 15;
        joke.rating = 0;

        var image = {};
        image.id = 2;
        image.type = "cuser:ImageItem";
        image.source = "9gag";
        image.url = "http://img-9gag-fun.9cache.com/photo/aDozWQ7_700b.jpg";
        image.title = "Truth has been spoken";
        image.description = "Truth has been spoken";
        image.src = "http://img-9gag-fun.9cache.com/photo/aDozWQ7_700b.jpg";
        image.reviewsCount = 12;
        image.rating = 2;

        var audio = {};
        audio.id = 3;
        audio.type = "cuser:AudioItem";
        audio.source = "CuSEr in collaboration with Google";
        audio.url = "http://www.manythings.org/jokes/9980.mp3";
        audio.title = "Good News & Bad News";
        audio.src = "http://www.manythings.org/jokes/9980.mp3";
        audio.reviewsCount = 2;
        audio.rating = 3;

        var video = {};
        video.id = 4;
        video.type = "cuser:VideoItem";
        video.source = "Youtube";
        video.url = "https://www.youtube.com/watch?v=BlFdAm2AjAk";
        video.title = "The Most Absurd Jokes & Funny Stories From A Life";
        video.src = "https://www.youtube.com/embed/Hf3_hhfJ8Hc";
        video.reviewsCount = 0;
        video.rating = 5;

        items.push(joke);
        items.push(image);
        items.push(audio);
        items.push(video);

        return items;
    };

    var shuffle = function (array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    };

    var distinct = function (items) {
        var set = {};
        var distinctItems = [];
        for (var i = 0; i < items.length; i++) {
            if (!set[items[i].id]) {
                distinctItems.push(items[i]);
                set[items[i].id] = true;
            }
        }

        return distinctItems;
    };

    var comparerFn = function (a, b) {
        var ratingDiff = b.rating - a.rating;
        if (ratingDiff === 0) {
            return b.reviewsCount - a.reviewsCount;
        }

        return ratingDiff;
    };

    var saveItems = function (items, callback) {
        var SAVED = 0;
        var SHOULD_SAVE = items.length;

        var itemSaved = function () {
            SAVED++;
            if (SAVED >= SHOULD_SAVE) {
                callback(items);
            }
        };

        var saveJoke = function (jokeItem) {
            if (cache["item-" + jokeItem.url]) {
                jokeItem.id = cache["item-" + jokeItem.url];
                itemSaved();
                return;
            }

            repository.insertJokeItem({
                id: jokeItem.id,
                iri: jokeItem.url,
                joke: jokeItem.joke,
                from: jokeItem.source
            }, function (item) {
                return function (res1) {
                    if (res1.paramsOk && res1.executed) {
                        item.id = res1.data.id;
                        cache["item-" + jokeItem.url] = item.id;
                        itemSaved();
                    }
                    else {
                        repository.selectJokeItemIdByIri({iri: item.url}, function (res2) {
                            item.id = res1.paramsOk && res1.found ? res2.data.id : 0;
                            cache["item-" + jokeItem.url] = item.id;
                            itemSaved();
                        });
                    }
                };
            }(jokeItem));
        };

        var saveImage = function (imageItem) {
            if (cache["item-" + imageItem.url]) {
                imageItem.id = cache["item-" + imageItem.url];
                itemSaved();
                return;
            }

            repository.insertImageItem({
                id: imageItem.id,
                iri: imageItem.url,
                title: imageItem.title,
                src: imageItem.src,
                fullDesc: imageItem.description,
                from: imageItem.source
            }, function (item) {
                return function (res1) {
                    if (res1.paramsOk && res1.executed) {
                        item.id = res1.data.id;
                        cache["item-" + imageItem.url] = item.id;
                        itemSaved();
                    }
                    else {
                        repository.selectImageItemIdByIri({iri: item.url}, function (res2) {
                            item.id = res1.paramsOk && res1.found ? res2.data.id : 0;
                            cache["item-" + imageItem.url] = item.id;
                            itemSaved();
                        });
                    }
                };
            }(imageItem));
        };

        var saveVideo = function (videoItem) {
            if (cache["item-" + videoItem.url]) {
                videoItem.id = cache["item-" + videoItem.url];
                itemSaved();
                return;
            }

            repository.insertVideoItem({
                id: videoItem.id,
                iri: videoItem.url,
                title: videoItem.title,
                src: videoItem.src,
                from: videoItem.source,
                hours: 0,
                minutes: 0,
                seconds: 0
            }, function (item) {
                return function (res1) {
                    if (res1.paramsOk && res1.executed) {
                        item.id = res1.data.id;
                        cache["item-" + videoItem.url] = item.id;
                        itemSaved();
                    }
                    else {
                        repository.selectVideoItemIdByIri({iri: item.url}, function (res2) {
                            item.id = res1.paramsOk && res1.found ? res2.data.id : 0;
                            cache["item-" + videoItem.url] = item.id;
                            itemSaved();
                        });
                    }
                };
            }(videoItem));
        };

        var saveAudio = function (audioItem) {
            if (cache["item-" + audioItem.url]) {
                audioItem.id = cache["item-" + audioItem.url];
                itemSaved();
                return;
            }

            repository.insertAudioItem({
                id: audioItem.id,
                iri: audioItem.url,
                title: audioItem.title,
                src: audioItem.src,
                from: audioItem.source
            }, function (item) {
                return function (res1) {
                    if (res1.paramsOk && res1.executed) {
                        item.id = res1.data.id;
                        cache["item-" + audioItem.url] = item.id;
                        itemSaved();
                    }
                    else {
                        repository.selectAudioItemIdByIri({iri: item.url}, function (res2) {
                            item.id = res1.paramsOk && res1.found ? res2.data.id : 0;
                            cache["item-" + audioItem.url] = item.id;
                            itemSaved();
                        });
                    }
                };
            }(audioItem));
        };

        for (var i = 0; i < items.length; i++) {
            switch (items[i].type) {
                case config.cuserClasses.jokeItem:
                    saveJoke(items[i]);
                    break;
                case config.cuserClasses.imageItem:
                    saveImage(items[i]);
                    break;
                case config.cuserClasses.videoItem:
                    saveVideo(items[i]);
                    break;
                case config.cuserClasses.audioItem:
                    saveAudio(items[i]);
                    break;
            }
        }
    };

    var setItemId = function (item) {
        item.id = randomService.getRandomId();
    };

    var setItemsIds = function (items) {
        for (var i = 0; i < items.length; i++) {
            if (cache["item-" + items[i].url]) {
                items[i].id = cache["item-" + items[i].url];
            }
            else {
                setItemId(items[i]);
            }

            items[i].rating = Math.floor((Math.random() * 5) + 1);
            items[i].reviewsCount = Math.floor((Math.random() * 100) + 0);
        }
    };

    var getItems = function (user, userLocation, product, offset, limit, callback) {
        var VIDEOS = [];
        var IMAGES = [];
        var JOKES = [];
        var SOUNDS = [];
        var RECOMMENDED = [];
        var LOADED = 0;
        var SHOULD_LOAD = user.likes.length * 2 + 5 + 1 + 1 + 10 + 10 + 1;

        console.log("[SHOULD_LOAD]" + SHOULD_LOAD);

        var itemsLoaded = function () {
            LOADED++;
            /*console.log("[VIDEOS]" + VIDEOS.length);
            console.log("[IMAGES]" + IMAGES.length);
            console.log("[JOKES]" + JOKES.length);
            console.log("[SOUNDS]" + SOUNDS.length);
            console.log("[RECOMMENDED]" + RECOMMENDED.length);*/
            if (LOADED >= SHOULD_LOAD) {
                var items = VIDEOS.concat(IMAGES).concat(JOKES).concat(SOUNDS);
                setItemsIds(items);
                items.sort(comparerFn);
                saveItems(items, function (items) {
                    console.log(items.length + " EntertainmentItems Synced with Db!");
                });
                var result = distinct(RECOMMENDED.concat(items));
                callback(result);
            }
        };

        userFeedbackService.getEntertainmentItemsByFriendsFeedback({email: user.email}, function (items) {
            RECOMMENDED = items;
            itemsLoaded();
        });

        incdbJokesService.getJokes(10, function (randomJokes) {
            JOKES = JOKES.concat(randomJokes);
            itemsLoaded();
        });

        incdbJokesService.getJokesByName(10, user.username, "", function (nameJokes) {
            JOKES = JOKES.concat(nameJokes);
            itemsLoaded();
        });

        incdbJokesService.getJokes(10, function (randomJokes) {
            for (var i = 0; i < randomJokes.length; i++) {
                googleAudioService.getAudioItem(randomJokes[i].joke, function (randomAudioJoke) {
                    SOUNDS.push(randomAudioJoke);
                    itemsLoaded();
                });
            }

            var foundDiff = 10 - randomJokes.length;
            if(foundDiff > 0){
                LOADED+=(foundDiff-1);
                itemsLoaded();
            }
        });

        incdbJokesService.getJokesByName(10, user.username, "", function (nameJokes) {
            for (var i = 0; i < nameJokes.length; i++) {
                googleAudioService.getAudioItem(nameJokes[i].joke, function (randomAudioJoke) {
                    SOUNDS.push(randomAudioJoke);
                    itemsLoaded();
                });
            }

            var foundDiff = 10 - nameJokes.length;
            if(foundDiff > 0){
                LOADED+=(foundDiff-1);
                itemsLoaded();
            }
        });

        gagService.getComics(user.username, function (nameComics) {
            IMAGES = IMAGES.concat(nameComics);
            itemsLoaded();
        });

        gagService.getComics(userLocation.pointOfInterest, function (locationComics) {
            IMAGES = IMAGES.concat(locationComics);
            itemsLoaded();
        });

        gagService.getComics(product.name, function (productComics) {
            IMAGES = IMAGES.concat(productComics);
            itemsLoaded();
        });

        youtubeService.getVideo(userLocation.pointOfInterest, 3, function (locationVideos) {
            VIDEOS = VIDEOS.concat(locationVideos);
            itemsLoaded();
        });

        youtubeService.getVideo(product.name, 3, function (productVideos) {
            VIDEOS = VIDEOS.concat(productVideos);
            itemsLoaded();
        });

        for (var i = 0; i < user.likes.length; i++) {
            youtubeService.getVideo(user.likes[i], 1, function (likeVideos) {
                VIDEOS = VIDEOS.concat(likeVideos);
                itemsLoaded();
            });
            gagService.getComics(user.likes[i], function (likeComics) {
                IMAGES = IMAGES.concat(likeComics.slice(0, 2));
                itemsLoaded();
            });
        }
    };

    var getItemById = function (id, callback) {
        repository.detectClassById({id: id}, function (res1) {
            if (!res1.paramsOk || !res1.found) {
                var result = {};
                result.success = false;
                result.message = "EntertainmentId not found! " + id;
                callback(result);
                return;
            }

            var itemType = config.cuserClasses.prefix + ":".concat(res1.data.type.slice(config.cuserClasses.cuser.length));

            switch (itemType) {
                case config.cuserClasses.jokeItem:
                    repository.selectJokeItemById({id: id}, function (result) {
                        var joke = {};
                        joke.id = id;
                        joke.type = itemType;
                        joke.url = result.data.iri;
                        joke.joke = result.data.joke;
                        joke.source = config.cuserClasses.prefix + ":".concat(result.data.from.slice(config.cuserClasses.cuser.length));
                        callback(joke);
                    });
                    break;
                case config.cuserClasses.imageItem:
                    repository.selectImageItemById({id: id}, function (result) {
                        var image = {};
                        image.id = id;
                        image.type = itemType;
                        image.url = result.data.iri;
                        image.title = result.data.title;
                        image.src = result.data.src;
                        image.description = result.data.fullDesc;
                        image.source = config.cuserClasses.prefix + ":".concat(result.data.from.slice(config.cuserClasses.cuser.length));
                        callback(image);
                    });
                    break;
                case config.cuserClasses.videoItem:
                    repository.selectVideoItemById({id: id}, function (result) {
                        var video = {};
                        video.id = id;
                        video.type = itemType;
                        video.url = result.data.iri;
                        video.title = result.data.title;
                        video.src = result.data.src;
                        video.source = config.cuserClasses.prefix + ":".concat(result.data.from.slice(config.cuserClasses.cuser.length));
                        callback(video);
                    });
                    break;
                case config.cuserClasses.audioItem:
                    repository.selectAudioItemById({id: id}, function (result) {
                        var audio = {};
                        audio.id = id;
                        audio.type = itemType;
                        audio.url = result.data.iri;
                        audio.title = result.data.title;
                        audio.src = result.data.src;
                        audio.source = config.cuserClasses.prefix + ":".concat(result.data.from.slice(config.cuserClasses.cuser.length));
                        callback(audio);
                    });
                    break;
                default:
                    callback(null);
            }
        });
    };

    return {
        getItems: getItems,
        getItemById: getItemById
    };
};

module.exports = entertainmentService;