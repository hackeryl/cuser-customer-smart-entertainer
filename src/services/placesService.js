/**
 * Created by Vasile on 25-Jan-16.
 */
var placesService = function (repository) {
    var savePlace = function (place, callback) {

        repository.insertPointOfInterest({
            name: place.pointOfInterest,
            lat: place.latitude,
            long: place.longitude
        }, function (result) {
            var res = {};

            if (!result.paramsOk) {
                res.success = false;
                res.message = "Params not ok";
                callback(res);
                return;
            }

            if (!result.executed) {
                res.success = false;
                res.message = "Not executed";
                callback(res);
                return;
            }

            res.success = true;
            res.message = place.name + " saved !";
            callback(res);
        });
    };

    return {
        savePlace: savePlace
    };
};

module.exports = placesService;