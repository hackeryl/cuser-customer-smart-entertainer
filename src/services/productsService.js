/**
 * Created by Vasile on 25-Jan-16.
 */
var productsService = function (repository) {
    var saveProduct = function (product, callback) {
        product.name = product.name.toLowerCase();

        repository.selectProductTimeAndIdByName({name: product.name}, function (res1) {

            var res = {};

            if (!res1.paramsOk) {
                res.success = false;
                res.message = "Params not ok";
                callback(res);
                return;
            }

            if (res1.found) {
                repository.updateProductTimeById({
                    id: res1.data.id,
                    hours: ((parseInt(res1.data.hours) + 0) / 2),
                    minutes: ((parseInt(res1.data.minutes) + product.preparingTime) / 2),
                    seconds: ((parseInt(res1.data.seconds) + 0) / 2)
                }, function (res2) {
                    if (!res2.paramsOk) {
                        res.success = false;
                        res.message = "Params not ok";
                        callback(res);
                        return;
                    }

                    if (!res2.executed) {
                        res.success = false;
                        res.message = "Not executed";
                        callback(res);
                    }

                    res.success = true;
                    res.message = product.name + " time was updated!";
                    callback(res);
                });
            } else {
                repository.insertProduct({
                    name: product.name,
                    description: "",
                    hours: 0,
                    minutes: product.preparingTime,
                    seconds: 0
                }, function (res2) {
                    if (!res2.paramsOk) {
                        res.success = false;
                        res.message = "Params not ok";
                        callback(res);
                        return;
                    }

                    if (!res2.executed) {
                        res.success = false;
                        res.message = "Not executed";
                        callback(res);
                        return;
                    }

                    res.success = true;
                    res.message = product.name + " saved!";
                    callback(res);
                });
            }
        });
    };

    return {
        saveProduct: saveProduct
    };
};

module.exports = productsService;