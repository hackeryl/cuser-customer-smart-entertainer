/**
 * Created by Vasile on 1/25/2016.
 */
var YouTube = require('youtube-node');
var config = require('../config');

var youTube = new YouTube();
var key = config.youtube.key;
youTube.setKey(key);

var youtubeService = function () {
    var getVideo = function (keyword, limit, callback) {
        youTube.search(keyword, limit, function (error, data) {
            if (error) {
                callback([]);
            }
            else {
                var videoList = [];
                for (var i = 0; i < data.items.length; i++) {
                    var video = {};
                    //video.id = data.items[i].id.videoId;
                    video.type = config.cuserClasses.videoItem;
                    video.source = config.cuserClasses.youtubeAPI;
                    video.url = config.youtube.url + data.items[i].id.videoId;
                    video.src = config.youtube.src + data.items[i].id.videoId;
                    video.title = data.items[i].snippet.title;
                    video.duration = {
                        hours: 2,
                        minutes: 3,
                        seconds: 4
                    };
                    videoList.push(video);
                }
                callback(videoList);
            }
        });
    };
    return {
        getVideo: getVideo
    }
};


module.exports = youtubeService;