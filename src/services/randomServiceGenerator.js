/**
 * Created by Stefan on 1/21/2016.
 */

exports.getRandomId = function () {
    "use strict";

    var randomNr = Math.random();

    var date = new Date();
    var components = [
        date.getYear(),
        date.getMonth(),
        date.getDate(),
        date.getHours(),
        date.getMinutes(),
        date.getSeconds(),
        date.getMilliseconds()
    ];

    var timestamp = components.join("");

    return timestamp + randomNr.toString().replace(/\./g, '');
};