/**
 * Created by Vasile on 1/25/2016.
 */
var gagApi = require('node-9gag');
var config = require('../config');

var gagService = function () {
    var getComics = function (keyword, callback) {
        gagApi.find(keyword, function (err, data) {
            if (err) {
                callback([]);
            }
            else {
                var imageList = [];
                for (var i = 0; i < data.result.length; i++) {
                    if (data.result[i].image) {
                        var image = {};
                        //image.id = data.result[i].id;
                        image.type = config.cuserClasses.imageItem;
                        image.source = config.cuserClasses.nineGagAPI;
                        image.src = data.result[i].image.replace("220x145", "700b");
                        image.title = data.result[i].title;
                        image.url = data.result[i].url;
                        image.Description = data.result[i].author + ' - ' + data.result[i].title;
                        imageList.push(image);
                    }

                }
                callback(imageList);
            }
        });
    };

    return {
        getComics: getComics
    }
};


module.exports = gagService;