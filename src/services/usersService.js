/**
 * Created by Vasile on 23-Jan-16.
 */
var usersService = function (repository) {

    var updateUser = function (user, callback) {

        repository.selectPersonIdByEmail({mail: user.email}, function (res1) {
            "use strict";

            var i, j;
            var result = {};

            if (!res1.paramsOk) {
                result.success = false;
                result.message = "Parameters not ok!" + user.email;
                callback(result);
                return;
            }

            if (!res1.found) {
                repository.insertPerson({
                    firstName: user.username,
                    lastName: " ",
                    age: 1,
                    mail: user.email
                }, function (res2) {

                    for (i = 0; i < user.likes.length; i++) {
                        repository.insertLikeForPerson({id: res2.data.id, like: user.likes[i]}, function () {
                        });
                    }

                    for (j = 0; j < user.friends.length; j++) {

                        console.log(JSON.stringify(user.friends[j]));
                        repository.selectPersonIdByEmail({mail: user.friends[j].email}, function(res3){

                            if(res3.found) {

                                repository.addFriends({personId1: res2.data.id, personId2: res3.data.id}, console.log);
                            }
                        });
                    }

                    console.log("updateUser(created):" + res2);

                    result.success = true;
                    result.message = user.email + " created!";
                    callback(result);
                });
            } else {
                repository.updatePersonById({
                    id: res1.data.id,
                    firstName: user.username,
                    lastName: " ",
                    age: 1
                }, function (res2) {

                    for (i = 0; i < user.likes.length; i++) {
                        repository.insertLikeForPerson({id: res1.data.id, like: user.likes[i]}, function () {
                        });
                    }

                    /*  TODO: add friends when we login, if they have account on our app
                     *   Fb returns email right at login and at friend list get
                     *   Google return email right when we login only
                     *   Twitter never returns login
                     */
                    for (j = 0; j < user.friends.length; j++) {

                        console.log(JSON.stringify(user.friends[j]));
                        repository.selectPersonIdByEmail({mail: user.friends[j].email}, function(res3){

                            if(res3.found) {

                                repository.addFriends({personId1: res1.data.id, personId2: res3.data.id}, console.log);
                            }
                        });
                    }

                    console.log("updateUser(updated):" + res2);

                    result.success = true;
                    result.message = user.email + " updated!";
                    callback(result);
                });
            }
        });
    };

    return {
        updateUser: updateUser
    };
};

module.exports = usersService;