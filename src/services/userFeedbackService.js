/**
 * Created by Stefan on 1/29/2016.
 */
var config = require("../config");

var userFeedbackService = function (repository) {
    var addUserFeedback = function (userFeedback, callback) {

        var email = userFeedback.email,
            id = userFeedback.id,
            rating = userFeedback.rating,
            review = userFeedback.review;

        var result = {};

        repository.selectPersonIdByEmail({mail: email}, function (res1) {
            "use strict";

            if (!res1.paramsOk) {
                result.success = false;
                result.message = "Parameters not ok!" + email;
                callback(result);
                return;
            }

            if (!res1.found) {
                result.success = false;
                result.message = "User not found" + email;
            } else {

                repository.detectClassById({id: id}, function (res2) {

                    if (!res2.paramsOk) {
                        result.success = false;
                        result.message = "Parameters not ok!" + id;
                        callback(result);
                        return;
                    }

                    if (!res2.found) {
                        result.success = false;
                        result.message = "EntertainmentId not found!" + id;
                        callback(result);
                        return;
                    }

                    var currentType = config.cuserClasses.prefix + ":".concat(res2.data.type.slice(config.cuserClasses.cuser.length));

                    repository.insertUserFeedback({
                        userId: res1.data.id,
                        itemId: id,
                        rdfClass: currentType,
                        text: review,
                        rating: rating
                    }, function (res3) {

                        result.success = true;
                        result.message = "Feedback added!" + id;
                        result.data = {};
                        result.data.id = res3.data.id;
                        callback(result);
                    });
                });
            }
        });
    };

    var getUserFeedback = function (data, callback) {

        var id = data.id;

        repository.selectUserFeedbackById({
            id: id
        }, function (res1) {

            var result = {};

            if (!res1.paramsOk) {
                result.success = false;
                result.message = "Parameters not ok! " + id;
                callback(result);
                return;
            }

            if (!res1.found) {
                result.success = false;
                result.message = "User feedback not found " + id;
                callback(result);
            } else {
                result.success = true;
                result.message = "User feedback found " + id;
                result.data = res1.data;
                callback(result);
            }
        });
    };

    var getFeedbackIdsList = function (data, callback) {
        repository.getIdsListForEntertainmentItem({
            id: data.id
        }, function (res1) {

            var result = {};

            if (!res1.paramsOk) {
                result.success = false;
                result.message = "Parameters not ok! " + data.id;
                callback(result);
                return;
            }

            if (!res1.found) {
                result.success = false;
                result.message = "User feedback ids not found " + data.id;
                callback(result);
            } else {
                result.success = true;
                result.message = "User feedback ids found " + data.id;
                result.data = res1.data;
                callback(result);
            }
        });
    };

    var getEntertainmentItemsByFriendsFeedback = function (data, callback) {

        var RESULTS = [];
        var LOADED = 0;
        var SHOULD_LOAD;

        repository.getIdsListForEntertainmentItemLikedByFriends({
            email: data.email
        }, function (res1) {
            if (!res1.paramsOk || !res1.found) {
                callback([]);
                return;
            }

            SHOULD_LOAD = res1.data.length;

            for (var i = 0; i < res1.data.length; i++) {

                (function (aux) {
                    repository.detectClassById({id: aux}, function (res2) {

                        var currentType = config.cuserClasses.prefix + ":".concat(res2.data.type.slice(config.cuserClasses.cuser.length));

                        switch (currentType) {
                            case config.cuserClasses.audioItem:
                                repository.selectAudioItemById({id: aux}, function (result) {
                                    RESULTS.push({
                                        id: aux,
                                        type: config.cuserClasses.audioItem,
                                        url: result.data.iri,
                                        source: config.cuserClasses.prefix + ":".concat(result.data.from.slice(config.cuserClasses.cuser.length)),
                                        title: result.data.title,
                                        src: result.data.src
                                    });
                                    LOADED++;
                                    if (LOADED == SHOULD_LOAD) {
                                        callback(RESULTS);
                                    }
                                });
                                break;
                            case config.cuserClasses.videoItem:
                                repository.selectVideoItemById({id: aux}, function (result) {
                                    RESULTS.push({
                                        id: aux,
                                        type: config.cuserClasses.videoItem,
                                        url: result.data.iri,
                                        source: config.cuserClasses.prefix + ":".concat(result.data.from.slice(config.cuserClasses.cuser.length)),
                                        title: result.data.title,
                                        src: result.data.src
                                    });
                                    LOADED++;
                                    if (LOADED == SHOULD_LOAD) {
                                        callback(RESULTS);
                                    }
                                });
                                break;
                            case config.cuserClasses.jokeItem:
                                repository.selectJokeItemById({id: aux}, function (result) {
                                    RESULTS.push({
                                        id: aux,
                                        type: config.cuserClasses.jokeItem,
                                        url: result.data.iri,
                                        source: config.cuserClasses.prefix + ":".concat(result.data.from.slice(config.cuserClasses.cuser.length)),
                                        joke: result.data.joke
                                    });
                                    LOADED++;
                                    if (LOADED == SHOULD_LOAD) {
                                        callback(RESULTS);
                                    }
                                });
                                break;
                            case config.cuserClasses.imageItem:
                                repository.selectImageItemById({id: aux}, function (result) {
                                    RESULTS.push({
                                        id: aux,
                                        type: config.cuserClasses.imageItem,
                                        url: result.data.iri,
                                        source: config.cuserClasses.prefix + ":".concat(result.data.from.slice(config.cuserClasses.cuser.length)),
                                        title: result.data.title,
                                        src: result.data.src,
                                        description: result.data.fullDesc
                                    });
                                    LOADED++;
                                    if (LOADED == SHOULD_LOAD) {
                                        callback(RESULTS);
                                    }
                                });
                                break;
                        }
                    });
                })(res1.data[i].itemId);
            }
        });
    };

    return {
        addUserFeedback: addUserFeedback,
        getUserFeedback: getUserFeedback,
        getFeedbackIdsList: getFeedbackIdsList,
        getEntertainmentItemsByFriendsFeedback: getEntertainmentItemsByFriendsFeedback
    };
};

module.exports = userFeedbackService;