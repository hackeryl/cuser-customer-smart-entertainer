/**
 * Created by Stefan on 1/24/2016.
 */
var config = require("../config");

var apiService = function (repository) {
    var getEntertainmentItems = function (callback) {

        repository.selectEntertainmentItems({offset: 0, limit: 100}, function (res) {

            var i, currentType;

            var items = [];

            for (i = 0; i < res.data.length; i++) {

                currentType = config.cuserClasses.prefix + ":".concat(res.data[i].type.slice(config.cuserClasses.cuser.length));

                switch (currentType) {
                    case config.cuserClasses.videoItem:
                        var videoItem = {
                            id: res.data[i].id,
                            type: res.data[i].type,
                            source: res.data[i].source,
                            url: res.data[i].iri,
                            duration: {
                                hours: res.data[i].hours,
                                minutes: res.data[i].minutes,
                                seconds: res.data[i].seconds
                            },
                            title: res.data[i].title
                        };
                        items.push(videoItem);
                        break;
                    case config.cuserClasses.audioItem:
                        var audioItem = {
                            id: res.data[i].id,
                            type: res.data[i].type,
                            source: res.data[i].source,
                            url: res.data[i].iri,
                            duration: {
                                hours: res.data[i].hours,
                                minutes: res.data[i].minutes,
                                seconds: res.data[i].seconds
                            },
                            title: res.data[i].title
                        };
                        items.push(audioItem);
                        break;
                    case config.cuserClasses.jokeItem:
                        var jokeItem = {
                            id: res.data[i].id,
                            type: res.data[i].type,
                            source: res.data[i].source,
                            url: res.data[i].url,
                        };
                        items.push(jokeItem);
                        break;
                    case config.cuserClasses.imageItem:
                        var imageItem = {
                            id: res.data[i].id,
                            type: res.data[i].type,
                            source: res.data[i].source,
                            url: res.data[i].url,
                            title: res.data[i].title,
                            shortDescription: res.data[i].shortDesc,
                            fullDescription: res.data[i].fullDesc,
                        };
                        items.push(imageItem);
                        break;
                }
            }
            callback(items);
        });
    };

    var test = function (callback) {

        console.log("testttt");

        repository.insertJokeItem({
            iri: "http://google.com",
            joke: "lol....",
            from: "cuser:9GagAPI"
        }, function (result) {

        });

        repository.insertAudioItem({
            iri: "https://www.youtube.com/watch?v=dNS9iKYRYc0",
            from: "cuser:YoutubeAPI",
            title: "Ooooo title",
            src: "srccc"
        }, function (result) {

        });

        repository.insertVideoItem({
            iri: "https://www.youtube.com/watch?v=I5f3grxJPdk",
            src: "SRCCCC",
            hours: 1,
            minutes: 8,
            seconds: 8,
            from: "cuser:YoutubeAPI",
            title: "Titlu faaain"
        }, function (result) {

        });

        repository.insertImageItem({
            iri: "https://www.youtube.com/watch?v=I5f3grxJPdk",
            src: "image src",
            title: "Imagine faina",
            from: "cuser:9GagAPI"
        }, function (result) {

        });

        callback({
            done: false
        });
    };

    var addUser = function (data, callback) {

    };
    var getUser = function (data, callback) {

        repository.selectPersonById({id: data.id}, function(ret1){

            if(!ret1.paramsOk || !ret1.found) {

                callback(undefined);
                return;
            }

            repository.constructPersonById({
                id: data.id,
                node: ret1.data.node,
                firstName: ret1.data.firstName,
                mbox: ret1.data.mbox
            }, function(ret2){
                callback(ret2);
            });
        });
    };
    var updateUser = function(data, callback) {

        repository.selectPersonIdByEmail({mail: data.email}, function (res1) {
            "use strict";

            var result = {};

            if (!res1.paramsOk) {
                result.paramsOk = false;
                callback(result);
                return;
            }

            if (!res1.found) {
                result.paramsOk = true;
                result.found = false;
                result.fraud = false;
                callback(result);
            } else {
                if(data.id !== res1.data.id) {
                    result.paramsOk = true;
                    result.found = true;
                    result.fraud = true;
                    callback(result);
                    return;
                }

                repository.updatePersonById({
                    id: data.id,
                    firstName: data.firstName
                }, function (res2) {
                    result.paramsOk = true;
                    result.found = true;
                    result.fraud = false;
                    callback(result);
                });
            }
        });
    };

    return {
        getEntertainmentItems: getEntertainmentItems,
        test: test,

        addUser: addUser,
        getUser: getUser,
        updateUser: updateUser
    };
};

module.exports = apiService;