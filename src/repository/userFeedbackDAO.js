/**
 * Created by Stefan on 1/23/2016.
 */
var utils = require('../services/randomServiceGenerator');
var check = require('check-types');
var DatabaseManager = require('./stardogDatabaseManager');

function UserFeedbackDAO(conn, databaseName) {
    "use strict";

    this.conn = conn;
    this.databaseName = databaseName;
}

/*
 { userId: string, itemId: string, rdfClass: string, text: string, rating: number, like: boolean }, function(object)

 { paramsOk: boolean, executed: boolean, data: { id } }
 */
UserFeedbackDAO.prototype.insert = function (data, fun) {
    var paramsOk = true, executed = true;

    if (!check.string(data.userId) || check.emptyString(data.userId)) paramsOk = false;
    if (!check.string(data.itemId) || check.emptyString(data.itemId)) paramsOk = false;
    if (!check.string(data.rdfClass) || check.emptyString(data.rdfClass)) paramsOk = false;
    if (!check.string(data.text) || check.emptyString(data.text)) data.text = " ";
    if (!check.number(data.rating))  data.rating = -1;
    //if (!check.boolean(data.like))   paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            executed: false,
            data: undefined
        });
        return;
    }

    var id = utils.getRandomId();

    var what1 =
        "  [] rdf:type         cuser:UserFeedback;\n" +
        "     ns:id            \"" + id + "\";\n" +
        "     cuser:suppliedBy ?user;\n" +
        "     cuser:basedOn    ?item;\n" +
        "     cuser:feedback   \"" + data.text + "\";\n" +
        "     dbo:rating        " + data.rating + "\n";
    //"     cuser:like       " + data.like;

    var conditions1 =
        "      ?user rdf:type       foaf:Person;\n" +
        "            ns:id          \"" + data.userId + "\".\n" +
        "      ?item rdf:type       " + data.rdfClass + ";\n" +
        "            ns:id          \"" + data.itemId + "\"";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildInsertWhere(what1, conditions1)
        },
        function (data) {
            var result = {
                paramsOk: true,
                executed: check.undefined(data['boolean']) ? false : data['boolean'],
                data: {
                    id: check.undefined(data['boolean']) ? undefined :
                        ( data['boolean'] ? id : undefined )
                }
            };

            fun(result);
        });
};

/*
 { id: string }, function(object)

 { paramsOk: boolean, found: boolean, data: { name desc hours min sec } }
 */
UserFeedbackDAO.prototype.selectById = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.string(data.id) || check.emptyString(data.id)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            found: false,
            data: undefined
        });
        return;
    }

    var what = "?itemId ?personId ?text ?rating";

    var conditions =
        "?s rdf:type         cuser:UserFeedback;\n" +
        "             ns:id            \"" + data.id + "\".\n" +
        "           OPTIONAL { ?s  cuser:feedback   ?text } .\n" +
        "           OPTIONAL { ?s dbo:rating      ?rating } .\n" +
        "           ?s  cuser:basedOn    ?item.\n" +
        "            ?s  cuser:suppliedBy ?person.\n" +
        "\n" +
        "       ?item   ns:id       ?itemId.\n" +
        "       ?person ns:id       ?personId";


    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildSelectWhere(what, conditions)
        },
        function (data) {
            var result;

            if (data.results.bindings.length == 0) {
                result = {
                    paramsOk: true,
                    found: false,
                    data: undefined
                };

                fun(result);
                return;
            }
            result = data.results.bindings[0];

            result = {
                paramsOk: true,
                found: true,
                data: {
                    itemId: result['itemId']['value'],
                    personId: result['personId']['value'],
                    text: check.undefined(result['text']) ? undefined : result['text']['value'],
                    rating: check.undefined(result['rating']) ? undefined : result['rating']['value']
                }
            };

            fun(result);
        });
};

/*
 { id: string, text: string, rating: int, like: boolean }, function(object)

 { paramsOk: boolean, executed: boolean }
 */
UserFeedbackDAO.prototype.updateById = function (data, fun) {
    var paramsOk = true, executed = true;

    var deleteData = "", insertData = "", whereData = "";

    if (!check.string(data.id) || check.emptyString(data.id)) paramsOk = false;
    else {
        whereData =
            "  ?s rdf:type         cuser:UserFeedback;\n" +
            "     ns:id            \"" + data.id + "\".";
    }
    if (!check.function(fun)) paramsOk = false;

    if (!check.undefined(data.text)) {

        if (!check.string(data.text) || check.emptyString(data.text)) paramsOk = false;
        else {
            deleteData = deleteData.concat("  ?s cuser:feedback   ?o1.\n");
            insertData = insertData.concat("?s cuser:feedback   \"" + data.text + "\".\n");
            whereData = whereData.concat("?s cuser:feedback   ?o1.\n");
        }
    }

    if (!check.undefined(data.rating)) {

        if (!check.number(data.rating)) paramsOk = false;
        else {
            deleteData = deleteData.concat("  ?s dbo:rating       ?o2.\n");
            insertData = insertData.concat("  ?s dbo:rating       " + data.rating + "   .\n");
            whereData = whereData.concat("  ?s dbo:rating       ?o2.\n");
        }
    }

    if (!check.undefined(data.like)) {

        if (!check.boolean(data.like)) paramsOk = false;
        else {
            deleteData = deleteData.concat("  ?s cuser:like       ?o3.\n");
            insertData = insertData.concat("  ?s cuser:like       " + data.like + ".\n");
            whereData = whereData.concat("  ?s cuser:like       ?o3.\n");
        }
    }

    if (check.emptyString(deleteData)) {
        deleteData = "nothing";
    }

    if (check.emptyString(insertData)) {
        insertData = "nothing";
    }

    if (!paramsOk) {
        fun({
            paramsOk: false,
            executed: false
        });
        return;
    }
    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildDeleteInsertWhere(deleteData, insertData, whereData)
        },
        function (data) {
            var result = {
                paramsOk: true,
                executed: check.undefined(data['boolean']) ? false : data['boolean']
            };

            fun(result);
        });
};

UserFeedbackDAO.prototype.getIdsListForEntertainmentItem = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.string(data.id) || check.emptyString(data.id)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            found: false,
            data: undefined
        });
        return;
    }

    var what = "?id";

    var conditions =
        "  ?s rdf:type ?type.\n" +
        "  ?type rdfs:subClassOf* cuser:EntertainmentItem.\n" +
        "  ?s ns:id \"" + data.id + "\".\n" +
        "  \n" +
        "  ?feedback cuser:basedOn ?s;\n" +
        "            ns:id ?id";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildSelectWhere(what, conditions)
        },
        function (data) {
            var result, i;

            var arrOfIds = [];

            if (data.results.bindings.length == 0) {
                result = {
                    paramsOk: true,
                    found: false,
                    data: undefined
                };

                fun(result);
                return;
            }

            result = data.results.bindings;

            for (i = 0; i < result.length; i++) {

                arrOfIds.push({
                    id: result[i]['id']['value']
                });
            }

            result = {
                paramsOk: true,
                found: true,
                data: arrOfIds
            };

            fun(result);
        });

};

UserFeedbackDAO.prototype.getIdsListForEntertainmentItemLikedByFriends = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.string(data.email) || check.emptyString(data.email)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            found: false,
            data: undefined
        });
        return;
    }

    var what = "?itemId ?friendFirstName ?friendId";

    var conditions =
        "  ?p rdf:type foaf:Person;\n" +
        "     foaf:mbox \"" + data.email + "\".\n" +
        "  \n" +
        "  ?friend rdf:type foaf:Person;\n" +
        "  \t\t  foaf:firstName ?friendFirstName;\n" +
        "          ns:id ?friendId.\n" +
        "  ?p foaf:knows ?friend.\n" +
        "  \n" +
        "  ?s rdf:type cuser:UserFeedback;\n" +
        "     cuser:suppliedBy ?friend;\n" +
        "     cuser:basedOn ?item;\n" +
        "     dbo:rating ?rating.\n" +
        "  \n" +
        "  ?item ns:id ?itemId.\n" +
        "  FILTER (?rating >= 2.5)";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildSelectWhere(what, conditions).concat("\n ORDER BY DESC(?rating)\n")
        },
        function (data) {
            var result, i;

            var arrOfIds = [];

            if (data.results.bindings.length == 0) {
                result = {
                    paramsOk: true,
                    found: false,
                    data: undefined
                };

                fun(result);
                return;
            }

            result = data.results.bindings;

            for (i = 0; i < result.length; i++) {

                arrOfIds.push({
                    itemId: result[i]['itemId']['value'],
                    friendFirstName: result[i]['friendFirstName']['value'],
                    friendId: result[i]['friendId']['value']
                });
            }

            result = {
                paramsOk: true,
                found: true,
                data: arrOfIds
            };

            fun(result);
        });

};

module.exports = UserFeedbackDAO;