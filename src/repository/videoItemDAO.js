/**
 * Created by Stefan on 1/23/2016.
 */
var utils = require('../services/randomServiceGenerator');
var check = require('check-types');
var DatabaseManager = require('./stardogDatabaseManager');

function VideoItem(conn, databaseName) {
    "use strict";

    this.conn = conn;
    this.databaseName = databaseName;
}

/*
 { iri: string, src: string, hours: int, minutes: int, seconds: int, from: string, title: string }, function(object)

 { paramsOk: boolean, executed: boolean, data: { id } }
 */
VideoItem.prototype.insert = function (data, fun) {
    var paramsOk = true, executed = true;

    if (!check.string(data.iri) || check.emptyString(data.iri)) paramsOk = false;
    if (!check.string(data.src) || check.emptyString(data.src)) paramsOk = false;
    if (!check.string(data.title) || check.emptyString(data.title)) paramsOk = false;
    if (!check.number(data.hours))   paramsOk = false;
    if (!check.number(data.minutes))   paramsOk = false;
    if (!check.number(data.seconds))   paramsOk = false;
    if (!check.string(data.from) || check.emptyString(data.from)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            executed: false,
            data: undefined
        });
        return;
    }

    var id = data.id ? data.id : utils.getRandomId();

    var what1 =
        "  [] rdf:type       time:DurationDescription;\n" +
        "     time:hours   " + data.hours + ";\n" +
        "     time:minutes   " + data.minutes + ";\n" +
        "     time:seconds   " + data.seconds;

    var conditions1 =
        "   FILTER NOT EXISTS {\n" +
        "      [] rdf:type       time:DurationDescription;\n" +
        "     time:hours   " + data.hours + ";\n" +
        "     time:minutes   " + data.minutes + ";\n" +
        "     time:seconds   " + data.seconds + "\n" +
        "  }";

    var what2 =
        "  [] rdf:type         cuser:VideoItem;\n" +
        "     ns:id            \"" + id + "\";\n" +
        "     terms:URI        \"" + data.iri + "\";\n" +
        "     terms:source        \"" + data.src + "\";\n" +
        "     terms:title        \"" + data.title + "\";\n" +
        "     cuser:providedBy " + data.from + ";\n" +
        "     cuser:length     ?s";

    var conditions2 =
        "      ?s rdf:type       time:DurationDescription;\n" +
        "     time:hours   " + data.hours + ";\n" +
        "     time:minutes   " + data.minutes + ";\n" +
        "     time:seconds   " + data.seconds;

    var that = this;

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildInsertWhere(what1, conditions1)
        },
        function (data) {
            that.conn.query({
                    database: that.databaseName,
                    query: DatabaseManager.buildInsertWhere(what2, conditions2)
                },
                function (data) {
                    var result = {
                        paramsOk: true,
                        executed: check.undefined(data['boolean']) ? false : data['boolean'],
                        data: {
                            id: check.undefined(data['boolean']) ? undefined :
                                ( data['boolean'] ? id : undefined )
                        }
                    };

                    fun(result);
                });
        });
};

/*
 { id: string }, function(object)

 { paramsOk: boolean, found: boolean, data: { iri src from title hours minutes seconds } }
 */
VideoItem.prototype.selectById = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.string(data.id) || check.emptyString(data.id)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            found: false,
            data: undefined
        });
        return;
    }

    var what = "?IRI ?src ?from ?title ?hours ?minutes ?seconds";

    var conditions =
        "  [] rdf:type         cuser:VideoItem;\n" +
        "     ns:id            \"" + data.id + "\";\n" +
        "     terms:URI        ?IRI;\n" +
        "     terms:source        ?src;\n" +
        "     terms:title        ?title;\n" +
        "     cuser:providedBy ?from;\n" +
        "     cuser:length     ?t.\n" +
        "\n" +
        "  ?t rdf:type       time:DurationDescription;\n" +
        "     time:hours   ?hours;\n" +
        "     time:minutes ?minutes;\n" +
        "     time:seconds ?seconds";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildSelectWhere(what, conditions)
        },
        function (data) {
            var result;

            if (data.results.bindings.length == 0) {
                result = {
                    paramsOk: true,
                    found: false,
                    data: undefined
                };

                fun(result);
                return;
            }
            result = data.results.bindings[0];

            result = {
                paramsOk: true,
                found: true,
                data: {
                    iri: result['IRI']['value'],
                    src: result['src']['value'],
                    from: result['from']['value'],
                    hours: result['hours']['value'],
                    minutes: result['minutes']['value'],
                    seconds: result['seconds']['value'],
                    title: result['title']['value']
                }
            };

            fun(result);
        });
};

/*
 { iri: string }, function(object)

 { paramsOk: boolean, executed: boolean, data: { id } }
 */
VideoItem.prototype.selectIdByIri = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.string(data.iri) || check.emptyString(data.iri)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            found: false,
            data: undefined
        });
        return;
    }

    var what = "?id";

    var conditions =
        "  ?s rdf:type       cuser:VideoItem;\n" +
        "     terms:URI        \"" + data.iri + "\";\n" +
        "     ns:id          ?id\n";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildSelectWhere(what, conditions)
        },
        function (data) {
            var result;

            if (data.results.bindings.length == 0) {
                result = {
                    paramsOk: true,
                    found: false,
                    data: undefined
                };

                fun(result);
                return;
            }
            result = data.results.bindings[0];

            result = {
                paramsOk: true,
                found: true,
                data: {
                    id: result['id']['value']
                }
            };
            fun(result);
        });
};

/*
 //  offset and limit can be ommited
 { offset: unsigned | undefined, limit: unsigned | undefined }, function(object)

 { paramsOk: boolean, found: boolean, data: [ { id iri src from title hours minutes seconds nodeName type } ] ]
 */
VideoItem.prototype.selectAlItems = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            found: false,
            data: undefined
        });
    }

    var what = "?id ?iri ?src ?title ?hours ?minutes ?seconds ?from ?nodeName ?type";

    var conditions =
        "  ?nodeName rdf:type cuser:VideoItem;\n" +
        "    rdf:type ?type;\n" +
        "    terms:title ?title;\n" +
        "    terms:source ?src;\n" +
        "    ns:id ?id;\n" +
        "    terms:URI ?iri;\n" +
        "    cuser:length ?length;\n" +
        "    cuser:providedBy ?from.\n" +
        "  \n" +
        "  ?length time:hours ?hours;\n" +
        "          time:minutes ?minutes;\n" +
        "          time:seconds ?seconds";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildSelectWhere(what, conditions).concat(
                (check.number(data.offset) && check.greaterOrEqual(data.offset, 0)
                && check.number(data.limit) && check.greaterOrEqual(data.limit, 0) )
                    ? "OFFSET " + data.offset + " \n LIMIT " + data.limit : ""
            )
        },
        function (data) {
            try {
                var result, i;

                var arrOfItems = [];

                if (data.results.bindings.length == 0) {
                    result = {
                        paramsOk: true,
                        found: false,
                        data: undefined
                    };

                    fun(result);
                    return;
                }

                result = data.results.bindings;

                for (i = 0; i < result.length; i++) {

                    arrOfItems.push({
                        id: result[i]['id']['value'],
                        src: result[i]['src']['value'],
                        type: result[i]['type']['value'],
                        nodeName: result[i]['nodeName']['value'],
                        from: result[i]['from']['value'],
                        iri: result[i]['iri']['value'],
                        hours: result[i]['hours']['value'],
                        minutes: result[i]['minutes']['value'],
                        seconds: result[i]['seconds']['value'],
                        title: result[i]['title']['value']
                    });
                }

                result = {
                    paramsOk: true,
                    found: true,
                    data: arrOfItems
                };

                fun(result);
            } catch (err) {
                result = {
                    paramsOk: true,
                    found: false,
                    data: []
                };

                fun(result);
            }
        });
};

module.exports = VideoItem;