/**
 * Created by Stefan on 1/22/2016.
 */
var utils = require('../services/randomServiceGenerator');
var check = require('check-types');
var DatabaseManager = require('./stardogDatabaseManager');

function PointOfInterestDAO(conn, databaseName) {
    "use strict";

    this.conn = conn;
    this.databaseName = databaseName;
}

/*
 { name: string, lat: number, long: number }, function(object)

 { paramsOk: boolean, executed: boolean }
 */
PointOfInterestDAO.prototype.insert = function (data, fun) {
    var paramsOk = true, executed = true;

    if (!check.string(data.name) || check.emptyString(data.name)) paramsOk = false;
    if (!check.number(data.lat))   paramsOk = false;
    if (!check.number(data.long))   paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            executed: false,
            data: undefined
        });
        return;
    }

    var id = utils.getRandomId();

    var what1 =
        "  [] rdf:type          geo:Point;\n" +
        "     geo:lat           " + data.lat + ";\n" +
        "     geo:long          " + data.long;

    var conditions1 =
        "   FILTER NOT EXISTS {\n" +
        "     [] rdf:type       geo:Point;\n" +
        "        geo:lat        " + data.lat + ";\n" +
        "        geo:long       " + data.long + " }";

    var what2 =
        "  [] rdf:type          dbo:place;\n" +
        "     ns:id             \"" + id + "\";\n" +
        "     cuser:hasLocation ?s;\n" +
        "     dbo:information   \"" + data.name + "\"";

    var conditions2 =
        "  ?s rdf:type          geo:Point;\n" +
        "     geo:lat           " + data.lat + ";\n" +
        "     geo:long          " + data.long;

    var that = this;

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildInsertWhere(what1, conditions1)
        },
        function (data) {
            that.conn.query({
                    database: that.databaseName,
                    query: DatabaseManager.buildInsertWhere(what2, conditions2)
                },
                function (data) {
                    var result = {
                        paramsOk: true,
                        executed: check.undefined(data['boolean']) ? false : data['boolean'],
                        data: {
                            id: check.undefined(data['boolean']) ? undefined :
                                ( data['boolean'] ? id : undefined )
                        }
                    };

                    fun(result);
                });
        });
};

/*
 { id: string }, function(object)

 { paramsOk: boolean, found: boolean, data: { firstName, lastName, age, mbox } }
 */
PointOfInterestDAO.prototype.selectById = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.string(data.id) || check.emptyString(data.id)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            found: false,
            data: undefined
        });
        return;
    }

    var what = "?lat ?long ?info";

    var conditions =
        "  [] rdf:type dbo:place;\n" +
        "     ns:id    \"" + data.id + "\";\n" +
        "     dbo:information ?info;\n" +
        "     cuser:hasLocation ?loc.\n" +
        "\n" +
        "  ?loc rdf:type  geo:Point;\n" +
        "       geo:lat   ?lat;\n" +
        "       geo:long  ?long";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildSelectWhere(what, conditions)
        },
        function (data) {
            var result;

            if (data.results.bindings.length == 0) {
                result = {
                    paramsOk: true,
                    found: false,
                    data: undefined
                };

                fun(result);
                return;
            }
            result = data.results.bindings[0];

            result = {
                paramsOk: true,
                found: true,
                data: {
                    lat: result['lat']['value'],
                    long: result['long']['value'],
                    info: result['info']['value']
                }
            };

            fun(result);
        });
};

/*
 { latMin: number, latMax: number, longMin: number, longMax: number }, function(object)

 { paramsOk: boolean, found: boolean, data: [ { id, lat, long, info } ] }
 */
PointOfInterestDAO.prototype.selectLocationsInSquare = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.number(data.latMin))   paramsOk = false;
    if (!check.number(data.latMax))   paramsOk = false;
    if (!check.number(data.longMin))   paramsOk = false;
    if (!check.number(data.longMax))   paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            found: false,
            data: undefined
        });
        return;
    }

    var what = "?id ?lat ?long ?info";

    var conditions =
        "  [] rdf:type dbo:place;\n" +
        "     ns:id    ?id;\n" +
        "     dbo:information ?info;\n" +
        "     cuser:hasLocation ?loc.\n" +
        "\n" +
        "  ?loc rdf:type  geo:Point;\n" +
        "       geo:lat   ?lat;\n" +
        "       geo:long  ?long.\n" +
        "  \n" +
        "  FILTER ( ?lat >= " + data.latMin + " && ?lat <= " + data.latMax + " && ?long >= " + data.longMin + " && ?long <= " + data.longMax + " )";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildSelectWhere(what, conditions)
        },
        function (data) {
            var result, i;

            var arrOfPlaces = [];

            if (data.results.bindings.length == 0) {
                result = {
                    paramsOk: true,
                    found: false,
                    data: undefined
                };

                fun(result);
                return;
            }

            result = data.results.bindings;

            for (i = 0; i < result.length; i++) {

                arrOfPlaces.push({
                    id: result[i]['id']['value'],
                    lat: result[i]['lat']['value'],
                    long: result[i]['long']['value'],
                    info: result[i]['info']['value']
                });
            }

            result = {
                paramsOk: true,
                found: true,
                data: arrOfPlaces
            };

            fun(result);
        });
};

/*
 { id: string, information: string }, function(object)

 { paramsOk: boolean, executed: boolean }
 */
PointOfInterestDAO.prototype.updateById = function (data, fun) {
    var paramsOk = true, executed = true;

    var deleteData = "", insertData = "", whereData = "";

    if (!check.string(data.id) || check.emptyString(data.id)) paramsOk = false;
    else {
        whereData =
            "  ?p rdf:type dbo:place;\n" +
            "     ns:id    \"" + data.id + "\".";
    }
    if (!check.function(fun)) paramsOk = false;

    if (!check.undefined(data.information)) {

        if (!check.string(data.information) || check.emptyString(data.information)) paramsOk = false;
        else {
            deleteData = deleteData.concat("  ?p dbo:information ?o1\n");
            insertData = insertData.concat("  ?p dbo:information \"" + data.information + "\"\n");
            whereData = whereData.concat("  ?p dbo:information ?o1\n");
        }
    }

    if (check.emptyString(deleteData)) {
        deleteData = "nothing";
    }

    if (check.emptyString(insertData)) {
        insertData = "nothing";
    }

    if (!paramsOk) {
        fun({
            paramsOk: false,
            executed: false
        });
        return;
    }

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildDeleteInsertWhere(deleteData, insertData, whereData)
        },
        function (data) {
            var result = {
                paramsOk: true,
                executed: check.undefined(data['boolean']) ? false : data['boolean']
            };

            fun(result);
        });
};

/*
 { placeId: string, productId: string }, function(object)

 { paramsOk: boolean, executed: boolean }
 */
PointOfInterestDAO.prototype.addProductInStore = function (data, fun) {
    var paramsOk = true, executed = true;

    if (!check.number(data.placeId))   paramsOk = false;
    if (!check.number(data.productId))   paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            executed: false
        });
        return;
    }

    var what = "?place cuser:offers ?prod";

    var conditions =
        "        ?place rdf:type dbo:place;\n" +
        "               ns:id    \"" + data.placeId + "\".\n" +
        "        \n" +
        "        ?prod  rdf:type gr:ProductOrServiceModel;\n" +
        "               ns:id    \"" + data.productId + "\"";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildInsertWhere(what1, conditions1)
        },
        function (data) {
            var result = {
                paramsOk: true,
                executed: check.undefined(data['boolean']) ? false : data['boolean']
            };

            fun(result);
        });
};

module.exports = PointOfInterestDAO;