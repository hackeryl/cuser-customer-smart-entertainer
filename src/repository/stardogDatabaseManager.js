/**
 * Created by Stefan on 1/21/2016.
 */
var check = require('check-types');
var stardog = require("stardog");

var PersonDAO = require("./personDAO");
var PointOfInterestDAO = require("./pointOfInterestDAO");
var ProductDAO = require("./productDAO");
var UserFeedbackDAO = require("./userFeedbackDAO");
var JokeItemDAO = require("./jokeItemDAO");
var AudioItemDAO = require("./audioItemDAO");
var VideoItemDAO = require("./videoItemDAO");
var ImageItemDAO = require("./imageItemDAO");

function StardogDatabaseManager(data) {

    var url = data.url;
    var username = data.username;
    var password = data.password;
    var databaseName = data.databaseName;

    this.conn = new stardog.Connection();
    this.databaseName = databaseName;

    this.conn.setEndpoint(url);
    this.conn.setCredentials(username, password);

    this.personDAO = new PersonDAO(this.conn, databaseName);
    this.pointOfInterestDAO = new PointOfInterestDAO(this.conn, databaseName);
    this.productDAO = new ProductDAO(this.conn, databaseName);
    this.userFeedbackDAO = new UserFeedbackDAO(this.conn, databaseName);
    this.jokeItemDAO = new JokeItemDAO(this.conn, databaseName);
    this.audioItemDAO = new AudioItemDAO(this.conn, databaseName);
    this.videoItemDAO = new VideoItemDAO(this.conn, databaseName);
    this.imageItemDAO = new ImageItemDAO(this.conn, databaseName);
}

/*
 { id: string, rdfClass: string }, function(object)

 { paramsOk: boolean, found: boolean, data: { node } }
 */
StardogDatabaseManager.prototype.selectNodeByIdAndRDFClass = function (data, fun) {
    "use strict";

    var paramsOk = true, found = true;

    if (!check.string(data.id) || check.emptyString(data.id)) paramsOk = false;
    if (!check.string(data.rdfClass) || check.emptyString(data.rdfClass)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        return {
            paramsOk: false,
            found: false,
            data: undefined
        };
    }

    var what = "?s";

    var conditions =
        "  ?s rdf:type       " + data.rdfClass + ";\n" +
        "     ns:id      \"" + data.id + "\"\n";

    this.conn.query({
            database: databaseName,
            query: exports.buildSelectWhere(what, conditions)
        },
        function (data) {
            var result;

            if (data.results.bindings.length == 0) {
                result = {
                    paramsOk: true,
                    found: false,
                    data: undefined
                };

                fun(result);
                return;
            }
            result = data.results.bindings[0];

            result = {
                paramsOk: true,
                found: true,
                data: {
                    node: result['s']['value']
                }
            };

            fun(result);
        });
};
/*
 { name: string }, function(object)

 { paramsOk: boolean, executed: boolean }
 */
StardogDatabaseManager.prototype.deleteResourceByName = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.string(data.name) || check.emptyString(data.name)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        return {
            paramsOk: false,
            executed: false
        };
    }

    var what1 = "?s ?p <_:" + data.name + ">";
    var conditions1 = "?s ?p <_:" + data.name + ">";

    var what2 = "<_:" + data.name + "> ?p ?o";
    var conditions2 = "<_:" + data.name + "> ?p ?o";

    var that = this;

    this.conn.query({
            database: databaseName,
            query: exports.buildDeleteWhere(what1, conditions1)
        },
        function (data) {
            that.conn.query({
                    database: that.databaseName,
                    query: exports.buildDeleteWhere(what2, conditions2)
                },
                function (data) {
                    var result = {
                        paramsOk: true,
                        executed: check.undefined(data['boolean']) ? false : data['boolean']
                    };

                    fun(result);
                });
        });
};

/*
 { id: string, rdfClass: string }, function(object)

 { paramsOk: boolean, executed: boolean }
 */
StardogDatabaseManager.prototype.deleteResourceByIdAndClass = function (data, fun) {
    exports.selectNodeByIdAndRDFClass({id: data.id, rdfClass: data.rdfClass}, function (result) {
        "use strict";

        if (!result.paramsOk) {
            fun({
                paramsOk: false,
                executed: false
            });
            return;
        }

        if (!result.found) {
            fun({
                paramsOk: true,
                executed: false
            });
            return;
        }

        exports.deleteResourceByName({name: result.data.node}, fun);
    });
};

/*
 { node1: string, relation: string, node2: string }, function(object)

 { paramsOk: boolean, executed: boolean }
 */
StardogDatabaseManager.prototype.deleteRelationBetweenNodes = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.string(data.node1) || check.emptyString(data.node1)) paramsOk = false;
    if (!check.string(data.relation) || check.emptyString(data.relation)) paramsOk = false;
    if (!check.string(data.node2) || check.emptyString(data.node2)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        return {
            paramsOk: false,
            executed: false
        };
    }

    var what = "<_:" + data.node1 + "> " + data.relation + " <_:" + data.node2 + ">";
    var conditions = "<_:" + data.node1 + "> " + data.relation + " <_:" + data.node2 + ">";

    this.conn.query({
            database: databaseName,
            query: exports.buildDeleteWhere(what, conditions)
        },
        function (data) {
            var result = {
                paramsOk: true,
                executed: check.undefined(data['boolean']) ? false : data['boolean']
            };

            fun(result);
        });
};

/*
 { iri: string, rdfClass: string }, function(object)

 { paramsOk: boolean, found: boolean, data: { id } }
 */
StardogDatabaseManager.prototype.selectIdByIRIandRdfClass = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.string(data.iri) || check.emptyString(data.iri)) paramsOk = false;
    if (!check.string(data.rdfClass) || check.emptyString(data.rdfClass)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        return {
            paramsOk: false,
            found: false,
            data: undefined
        };
    }

    var what = "?id";

    var conditions =
        "  ?s rdf:type       " + data.rdfClass + ";\n" +
        "     cuser:IRI      \"" + data.iri + "\";\n" +
        "     ns:id          ?id\n";

    this.conn.query({
            database: databaseName,
            query: exports.buildSelectWhere(what, conditions)
        },
        function (data) {
            var result;

            if (data.results.bindings.length == 0) {
                result = {
                    paramsOk: true,
                    found: false,
                    data: undefined
                };

                fun(result);
                return;
            }
            result = data.results.bindings[0];

            result = {
                paramsOk: true,
                found: true,
                data: {
                    id: result['id']['value']
                }
            };

            fun(result);
        });
};

/*
 { offset: number, limit: number }, function(object)

 { paramsOk: boolean, found: boolean, data: { items: [ { ... } ] } }
 */
StardogDatabaseManager.prototype.selectEntertainmentItems = function (data, fun) {

    var result = {
        paramsOk: false,
        found: false,
        data: []
    };

    var that = this;

    this.audioItemDAO.selectAlItems(data, function (res1) {

        result.paramsOk |= res1.paramsOk;
        result.found |= res1.found;
        if (res1.found) {
            result.data = result.data.concat(res1.data);
        }

        that.videoItemDAO.selectAlItems(data, function (res2) {

            result.paramsOk |= res2.paramsOk;
            result.found |= res2.found;
            if (res2.found) {
                result.data = result.data.concat(res2.data);
            }

            that.imageItemDAO.selectAlItems(data, function (res3) {

                result.paramsOk |= res3.paramsOk;
                result.found |= res3.found;
                if (res3.found) {
                    result.data = result.data.concat(res3.data);
                }

                that.jokeItemDAO.selectAlItems(data, function (res4) {

                    result.paramsOk |= res4.paramsOk;
                    result.found |= res4.found;
                    if (res4.found) {
                        result.data = result.data.concat(res4.data);
                    }

                    fun(result);
                });
            });
        });
    });
};

module.exports = StardogDatabaseManager;

/*
 //  subject, predicate, object, prefix1, prefix2... (you can give no prefixes if you want)
 string, string, string, ...string

 string
 */
exports.buildTripleInsert = function (subject, predicate, object) {
    "use strict";

    if (check.undefined(subject)) subject = "paramNotPassed";
    if (check.undefined(predicate)) predicate = "paramNotPassed";
    if (check.undefined(object)) object = "paramNotPassed";

    var i;
    var insert = "";

    for (i = 3; i < arguments.length; i++) {
        insert.concat(arguments[i]).concat("\n");
    }

    return insert.concat(" INSERT DATA { ").concat(subject).concat(" ").concat(predicate).concat(" ")
        .concat(object).concat(" }\n");
};
exports.buildInsert = function (what) {
    "use strict";

    if (check.undefined(what)) what = "paramNotPassed";

    return "INSERT { ".concat(what).concat(" }\n");
};
exports.buildInsertData = function (what) {
    "use strict";

    if (check.undefined(what)) what = "paramNotPassed";

    return "INSERT DATA { ".concat(what).concat(" }\n");
};
exports.buildWhere = function (conditions) {
    "use strict";

    if (check.undefined(conditions)) conditions = "paramNotPassed";

    return "WHERE { ".concat(conditions).concat(" }\n");
};
exports.buildDelete = function (what) {
    "use strict";

    if (check.undefined(what)) what = "paramNotPassed";

    return "DELETE { ".concat(what).concat(" }\n");
};
exports.buildInsertWhere = function (what, conditions) {
    "use strict";

    return exports.buildInsert(what).concat(exports.buildWhere(conditions));
};
exports.buildSelectWhere = function (what, conditions) {
    "use strict";

    if (check.undefined(what)) what = "paramNotPassed";

    return "SELECT ".concat(what).concat(" \n").concat(exports.buildWhere(conditions));
};
exports.buildConstructWhere = function (conditions) {
    "use strict";

    return "CONSTRUCT \n".concat(exports.buildWhere(conditions));
};
exports.buildDeleteWhere = function (what, conditions) {
    "use strict";

    return exports.buildDelete(what).concat(exports.buildWhere(conditions));
};
exports.buildDeleteInsertWhere = function (toDelete, toInsert, conditions) {
    "use strict";

    return exports.buildDelete(toDelete).concat(exports.buildInsert(toInsert)).concat(exports.buildWhere(conditions));
};