/**
 * Created by Stefan on 1/23/2016.
 */
var utils = require('../services/randomServiceGenerator');
var check = require('check-types');
var DatabaseManager = require('./stardogDatabaseManager');

function ProductDAO(conn, databaseName) {
    "use strict";

    this.conn = conn;
    this.databaseName = databaseName;
}

/*
 { name: string, description: string, hours: int, minutes: int, seconds: int }, function(object)

 { paramsOk: boolean, executed: boolean, data: { id } }
 */
ProductDAO.prototype.insert = function (data, fun) {
    var paramsOk = true, executed = true;

    if (!check.string(data.name) || check.emptyString(data.name)) paramsOk = false;
    //if (!check.string(data.description) || check.emptyString(data.description)) paramsOk = false;
    if (!check.number(data.hours))   paramsOk = false;
    if (!check.number(data.minutes))   paramsOk = false;
    if (!check.number(data.seconds))   paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        return {
            paramsOk: false,
            executed: false,
            data: undefined
        };
    }

    var id = utils.getRandomId();

    var what1 =
        "  [] rdf:type       time:DurationDescription;\n" +
        "     time:hours   " + data.hours + ";\n" +
        "     time:minutes   " + data.minutes + ";\n" +
        "     time:seconds   " + data.seconds;

    var conditions1 =
        "   FILTER NOT EXISTS {\n" +
        "      [] rdf:type       time:DurationDescription;\n" +
        "     time:hours   " + data.hours + ";\n" +
        "     time:minutes   " + data.minutes + ";\n" +
        "     time:seconds   " + data.seconds + "\n" +
        "  }";

    var what2 =
        "  [] rdf:type              gr:ProductOrServiceModel;\n" +
        "     ns:id                 \"" + id + "\";\n" +
        "     gr:name               \"" + data.name + "\";\n" +
        "     gr:description        \"" + data.description + "\";\n" +
        "     cuser:hasDeliveryTime ?s";

    var conditions2 =
        "      ?s rdf:type       time:DurationDescription;\n" +
        "     time:hours   " + data.hours + ";\n" +
        "     time:minutes   " + data.minutes + ";\n" +
        "     time:seconds   " + data.seconds;

    var that = this;

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildInsertWhere(what1, conditions1)
        },
        function (data) {
            that.conn.query({
                    database: that.databaseName,
                    query: DatabaseManager.buildInsertWhere(what2, conditions2)
                },
                function (data) {
                    var result = {
                        paramsOk: true,
                        executed: check.undefined(data['boolean']) ? false : data['boolean'],
                        data: {
                            id: check.undefined(data['boolean']) ? undefined :
                                ( data['boolean'] ? id : undefined )
                        }
                    };

                    fun(result);
                });
        });
};

/*
 { id: string }, function(object)

 { paramsOk: boolean, found: boolean, data: { name desc hours min sec } }
 */
ProductDAO.prototype.selectById = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.string(data.id) || check.emptyString(data.id)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        return {
            paramsOk: false,
            found: false,
            data: undefined
        };
    }

    var what = "?name ?desc ?hours ?min ?sec";

    var conditions =
        "  ?s1 rdf:type             gr:ProductOrServiceModel;\n" +
        "     ns:id                 \"" + data.id + "\";\n" +
        "     gr:name               ?name;\n" +
        "     gr:description        ?desc;\n" +
        "     cuser:hasDeliveryTime ?t.\n" +
        "  \n" +
        "  ?t rdf:type       time:DurationDescription.\n" +
        "  ?t time:hours   ?hours.  \n" +
        "  ?t time:minutes ?min.\n" +
        "  ?t time:seconds ?sec.";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildSelectWhere(what, conditions)
        },
        function (data) {
            var result;

            if (data.results.bindings.length == 0) {
                result = {
                    paramsOk: true,
                    found: false,
                    data: undefined
                };

                fun(result);
                return;
            }
            result = data.results.bindings[0];

            result = {
                paramsOk: true,
                found: true,
                data: {
                    name: result['name']['value'],
                    desc: result['desc']['value'],
                    hours: result['hours']['value'],
                    minutes: result['min']['value'],
                    seconds: result['sec']['value']
                }
            };

            fun(result);
        });
};

/*
 { id: name }, function(object)

 { paramsOk: boolean, found: boolean, data: { id hours min sec } }
 */
ProductDAO.prototype.selectTimeAndIdByName = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.string(data.name) || check.emptyString(data.name)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        return {
            paramsOk: false,
            found: false,
            data: undefined
        };
    }

    var what = "?id ?hours ?min ?sec";

    var conditions =
        "  ?s1 rdf:type             gr:ProductOrServiceModel;\n" +
        "     ns:id ?id;\n" +
        "     gr:name                 \"" + data.name + "\";\n" +
        "     cuser:hasDeliveryTime ?t.\n" +
        "  \n" +
        "  ?t rdf:type       time:DurationDescription.\n" +
        "  ?t time:hours   ?hours.  \n" +
        "  ?t time:minutes ?min.\n" +
        "  ?t time:seconds ?sec.";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildSelectWhere(what, conditions)
        },
        function (data) {
            var result;

            if (data.results.bindings.length == 0) {
                result = {
                    paramsOk: true,
                    found: false,
                    data: undefined
                };

                fun(result);
                return;
            }
            result = data.results.bindings[0];

            result = {
                paramsOk: true,
                found: true,
                data: {
                    id: result['id']['value'],
                    hours: result['hours']['value'],
                    minutes: result['min']['value'],
                    seconds: result['sec']['value']
                }
            };

            fun(result);
        });
};

/*
 { id: string, hours: int, minutes: int, seconds: int }, function(object)

 { paramsOk: boolean, executed: boolean }
 */
ProductDAO.prototype.updateTimeById = function (data, fun) {
    var paramsOk = true, executed = true;

    if (!check.string(data.id) || check.emptyString(data.id)) paramsOk = false;
    if (!check.number(data.hours))   paramsOk = false;
    if (!check.number(data.minutes))   paramsOk = false;
    if (!check.number(data.seconds))   paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            executed: false
        });
        return;
    }

    var what1 = "?s cuser:hasDeliveryTime ?o1";

    var conditions1 =
        "  ?s rdf:type  gr:ProductOrServiceModel;\n" +
        "     ns:id     \"" + data.id + "\";\n" +
        "     cuser:hasDeliveryTime ?o1";

    var what2 =
        "  [] rdf:type       time:DurationDescription;\n" +
        "     time:hours   " + data.hours + ";\n" +
        "     time:minutes   " + data.minutes + ";\n" +
        "     time:seconds   " + data.seconds;

    var conditions2 =
        "   FILTER NOT EXISTS {\n" +
        "      [] rdf:type       time:DurationDescription;\n" +
        "     time:hours   " + data.hours + ";\n" +
        "     time:minutes   " + data.minutes + ";\n" +
        "     time:seconds   " + data.seconds + "\n" +
        "  }";

    var what3 = "?s cuser:hasDeliveryTime ?o";

    var conditions3 =
        "  ?s rdf:type  gr:ProductOrServiceModel;\n" +
        "     ns:id     \"" + data.id + "\".\n" +
        "  \n" +
        "  ?o rdf:type  time:DurationDescription;\n" +
        "     time:hours   " + data.hours + ";\n" +
        "     time:minutes   " + data.minutes + ";\n" +
        "     time:seconds   " + data.seconds;

    var that = this;

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildDeleteWhere(what1, conditions1)
        },
        function (data) {
            that.conn.query({
                    database: that.databaseName,
                    query: DatabaseManager.buildInsertWhere(what2, conditions2)
                },
                function (data) {
                    that.conn.query({
                            database: that.databaseName,
                            query: DatabaseManager.buildInsertWhere(what3, conditions3)
                        },
                        function (data) {
                            var result = {
                                paramsOk: true,
                                executed: check.undefined(data['boolean']) ? false : data['boolean']
                            };

                            fun(result);
                        });
                });
        });
};

module.exports = ProductDAO;