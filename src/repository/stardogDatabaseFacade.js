/**
 * Created by Stefan on 1/24/2016.
 */
var config = require("../config");
var StardogDatabaseManager = require('./stardogDatabaseManager');

var stardogDatabaseFacade = function () {
    "use strict";

    var db = new StardogDatabaseManager({
        url: config.stardog.url,
        username: config.stardog.username,
        password: config.stardog.password,
        databaseName: config.stardog.databaseName
    });

    var insertPerson = function (data, fn) {
        db.personDAO.insert(data, fn);
    };
    var selectPersonById = function (data, fn) {
        db.personDAO.selectById(data, fn);
    };
    var selectPersonIdByEmail = function (data, fn) {
        db.personDAO.selectIdByEmail(data, fn);
    };
    var updatePersonById = function (data, fn) {
        db.personDAO.updateById(data, fn);
    };
    var deletePersonById = function (data, fun) {
        db.deleteResourceByIdAndClass({id: data.id, rdfClass: config.cuserClasses.person}, fun);
    };

    var insertPointOfInterest = function (data, fn) {
        db.pointOfInterestDAO.insert(data, fn);
    };
    var selectPointOfInterestById = function (data, fn) {
        db.pointOfInterestDAO.selectById(data, fn);
    };
    var updatePointOfInterestById = function (data, fn) {
        db.pointOfInterestDAO.updateById(data, fn);
    };
    var deletePointOfInterestById = function (data, fun) {
        db.deleteResourceByIdAndClass({id: data.id, rdfClass: config.cuserClasses.pointOfInterest}, fun);
    };

    var insertProduct = function (data, fn) {
        db.productDAO.insert(data, fn);
    };
    var selectProductById = function (data, fn) {
        db.productDAO.selectById(data, fn);
    };
    var updateProductTimeById = function (data, fn) {
        db.productDAO.updateTimeById(data, fn);
    };
    var deleteProductById = function (data, fun) {
        db.deleteResourceByIdAndClass({id: data.id, rdfClass: config.cuserClasses.product}, fun);
    };

    //  entertainment items methods
    var insertAudioItem = function (data, fn) {
        db.audioItemDAO.insert(data, fn);
    };
    var selectAudioItemById = function (data, fn) {
        db.audioItemDAO.selectById(data, fn);
    };

    var insertVideoItem = function (data, fn) {
        db.videoItemDAO.insert(data, fn);
    };
    var selectVideoItemById = function (data, fn) {
        db.videoItemDAO.selectById(data, fn);
    };

    var insertJokeItem = function (data, fn) {
        db.jokeItemDAO.insert(data, fn);
    };
    var selectJokeItemById = function (data, fn) {
        db.jokeItemDAO.selectById(data, fn);
    };

    var insertImageItem = function (data, fn) {
        db.imageItemDAO.insert(data, fn);
    };

    var selectImageItemById = function (data, fn) {
        db.imageItemDAO.selectById(data, fn);
    };

    var insertUserFeedback = function (data, fn) {
        db.userFeedbackDAO.insert(data, fn);
    };

    var selectUserFeedbackById = function (data, fn) {
        db.userFeedbackDAO.selectById(data, fn);
    };

    var updateUserFeedbackById = function (data, fn) {
        db.userFeedbackDAO.updateById(data, fn);
    };

    var deleteUserFeedbackById = function (data, fun) {
        db.deleteResourceByIdAndClass({id: data.id, rdfClass: config.cuserClasses.userFeedback}, fun);
    };

    //  other methods
    var selectPointOfInterestsInSquare = function (data, fn) {
        db.pointOfInterestDAO.selectLocationsInSquare(data, fn);
    };
    var addProductInStore = function (data, fn) {
        db.pointOfInterestDAO.addProductInStore(data, fn);
    };

    var selectProductTimeAndIdByName = function (data, fn) {
        db.productDAO.selectTimeAndIdByName(data, fn);
    };

    var insertLikeForPerson = function (data, fn) {
        db.personDAO.insertLike(data, fn);
    };

    var getLikesOfPerson = function (data, fn) {
        db.personDAO.selectLikesById(data, fn);
    };

    var addFriends = function (data, fn) {
        db.personDAO.addFriends(data, fn)
    };

    var selectEntertainmentItems = function (data, fn) {
        db.selectEntertainmentItems(data, fn);
    };

    var detectClassById = function (data, fn) {
        db.personDAO.detectClassById(data, fn);
    };

    var getIdsListForEntertainmentItem = function (data, fn) {
        db.userFeedbackDAO.getIdsListForEntertainmentItem(data, fn);
    };

    var selectJokeItemIdByIri = function (data, fn) {
        db.jokeItemDAO.selectIdByIri(data, fn);
    };

    var selectAudioItemIdByIri = function (data, fn) {
        db.audioItemDAO.selectIdByIri(data, fn);
    };

    var selectVideoItemIdByIri = function (data, fn) {
        db.videoItemDAO.selectIdByIri(data, fn);
    };

    var selectImageItemIdByIri = function (data, fn) {
        db.imageItemDAO.selectIdByIri(data, fn);
    };

    var getIdsListForEntertainmentItemLikedByFriends =
        function (data, fn) {
            db.userFeedbackDAO.getIdsListForEntertainmentItemLikedByFriends(data, fn);
        };

    //  CONSTRUCT resources

    var constructPersonById = function (data, fn) {
        db.personDAO.constructRDF(data, fn);
    };

    return {
        insertPerson: insertPerson,
        selectPersonById: selectPersonById,
        selectPersonIdByEmail: selectPersonIdByEmail,
        updatePersonById: updatePersonById,
        deletePersonById: deletePersonById,
        insertPointOfInterest: insertPointOfInterest,
        selectPointOfInterestById: selectPointOfInterestById,
        updatePointOfInterestById: updatePointOfInterestById,
        deletePointOfInterestById: deletePointOfInterestById,
        insertProduct: insertProduct,
        selectProductById: selectProductById,
        updateProductTimeById: updateProductTimeById,
        deleteProductById: deleteProductById,
        insertAudioItem: insertAudioItem,
        selectAudioItemById: selectAudioItemById,
        insertVideoItem: insertVideoItem,
        selectVideoItemById: selectVideoItemById,
        insertJokeItem: insertJokeItem,
        selectJokeItemById: selectJokeItemById,
        insertUserFeedback: insertUserFeedback,
        selectUserFeedbackById: selectUserFeedbackById,
        updateUserFeedbackById: updateUserFeedbackById,
        deleteUserFeedbackById: deleteUserFeedbackById,
        selectPointOfInterestsInSquare: selectPointOfInterestsInSquare,
        addProductInStore: addProductInStore,
        insertLikeForPerson: insertLikeForPerson,
        getLikesOfPerson: getLikesOfPerson,
        addFriends: addFriends,
        selectEntertainmentItems: selectEntertainmentItems,
        insertImageItem: insertImageItem,
        selectImageItemById: selectImageItemById,
        selectProductTimeAndIdByName: selectProductTimeAndIdByName,
        detectClassById: detectClassById,
        getIdsListForEntertainmentItem: getIdsListForEntertainmentItem,
        selectJokeItemIdByIri: selectJokeItemIdByIri,
        selectAudioItemIdByIri: selectAudioItemIdByIri,
        selectVideoItemIdByIri: selectVideoItemIdByIri,
        selectImageItemIdByIri: selectImageItemIdByIri,

        getIdsListForEntertainmentItemLikedByFriends: getIdsListForEntertainmentItemLikedByFriends,

        constructPersonById: constructPersonById
    };
};

module.exports = stardogDatabaseFacade;