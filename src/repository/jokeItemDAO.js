/**
 * Created by Stefan on 1/23/2016.
 */
var utils = require('../services/randomServiceGenerator');
var check = require('check-types');
var DatabaseManager = require('./stardogDatabaseManager');

function JokeItemDAO(conn, databaseName) {
    "use strict";

    this.conn = conn;
    this.databaseName = databaseName;
}

/*
 { iri: string, joke: string, from string }, function(object)

 { paramsOk: boolean, executed: boolean, data: { id } }
 */
JokeItemDAO.prototype.insert = function (data, fun) {
    var paramsOk = true, executed = true;

    if (!check.string(data.iri) || check.emptyString(data.iri)) paramsOk = false;
    if (!check.string(data.joke) || check.emptyString(data.joke)) paramsOk = false;
    if (!check.string(data.from) || check.emptyString(data.from)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            executed: false,
            data: undefined
        });
        return;
    }

    var id = data.id ? data.id : utils.getRandomId();

    var what =
        "  [] rdf:type         cuser:JokeItem;\n" +
        "     ns:id            \"" + id + "\";\n" +
        "     cuser:providedBy " + data.from + ";\n" +
        "     terms:URI        \"" + data.iri + "\";\n" +
        "     cuser:joke       \"" + data.joke + "\"";

    var conditions =
        "    FILTER NOT EXISTS {\n" +
        "      [] rdf:type         cuser:JokeItem;\n" +
        "         terms:URI        \"" + data.iri + "\";\n" +
        "    }";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildInsertWhere(what, conditions)
        },
        function (data) {
            var result = {
                paramsOk: true,
                executed: check.undefined(data['boolean']) ? false : data['boolean'],
                data: {
                    id: check.undefined(data['boolean']) ? undefined :
                        ( data['boolean'] ? id : undefined )
                }
            };

            fun(result);
        });
};

/*
 { iri: string }, function(object)

 { paramsOk: boolean, executed: boolean, data: { id } }
 */
JokeItemDAO.prototype.selectIdByIri = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.string(data.iri) || check.emptyString(data.iri)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            found: false,
            data: undefined
        });
        return;
    }

    var what = "?id";

    var conditions =
        "  ?s rdf:type       cuser:JokeItem;\n" +
        "     terms:URI        \"" + data.iri + "\";\n" +
        "     ns:id          ?id\n";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildSelectWhere(what, conditions)
        },
        function (data) {
            var result;

            if (data.results.bindings.length == 0) {
                result = {
                    paramsOk: true,
                    found: false,
                    data: undefined
                };

                fun(result);
                return;
            }
            result = data.results.bindings[0];

            result = {
                paramsOk: true,
                found: true,
                data: {
                    id: result['id']['value']
                }
            };
            fun(result);
        });
};
/*
 { id: string }, function(object)

 { paramsOk: boolean, found: boolean, data: { iri from joke } }
 */
JokeItemDAO.prototype.selectById = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.string(data.id) || check.emptyString(data.id)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            found: false,
            data: undefined
        });
        return;
    }

    var what = "?IRI ?from ?joke";

    var conditions =
        "  [] rdf:type         cuser:JokeItem;\n" +
        "     ns:id            \"" + data.id + "\";\n" +
        "     terms:URI        ?IRI;\n" +
        "     cuser:providedBy ?from;\n" +
        "     cuser:joke       ?joke.";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildSelectWhere(what, conditions)
        },
        function (data) {
            var result;

            if (data.results.bindings.length == 0) {
                result = {
                    paramsOk: true,
                    found: false,
                    data: undefined
                };

                fun(result);
                return;
            }
            result = data.results.bindings[0];

            result = {
                paramsOk: true,
                found: true,
                data: {
                    iri: result['IRI']['value'],
                    from: result['from']['value'],
                    joke: result['joke']['value']
                }
            };

            fun(result);
        });
};

/*
 //  offset and limit can be ommited
 { offset: unsigned | undefined, limit: unsigned | undefined }, function(object)

 { paramsOk: boolean, found: boolean, data: [ { id iri from joke nodeName type } ] ]
 */
JokeItemDAO.prototype.selectAlItems = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            found: false,
            data: undefined
        });
        return;
    }

    var what = "?id ?iri ?joke ?from ?nodeName ?type";

    var conditions =
        "  ?nodeName rdf:type cuser:JokeItem;\n" +
        "    rdf:type ?type;\n" +
        "    ns:id ?id;\n" +
        "    terms:URI ?iri;\n" +
        "    cuser:joke ?joke;\n" +
        "    cuser:providedBy ?from";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildSelectWhere(what, conditions).concat(
                (check.number(data.offset) && check.greaterOrEqual(data.offset, 0)
                && check.number(data.limit) && check.greaterOrEqual(data.limit, 0) )
                    ? "OFFSET " + data.offset + " \n LIMIT " + data.limit : ""
            )
        },
        function (data) {
            try {
                var result, i;

                var arrOfItems = [];

                if (data.results.bindings.length == 0) {
                    result = {
                        paramsOk: true,
                        found: false,
                        data: undefined
                    };

                    fun(result);
                    return;
                }

                result = data.results.bindings;

                for (i = 0; i < result.length; i++) {

                    arrOfItems.push({
                        id: result[i]['id']['value'],
                        type: result[i]['type']['value'],
                        nodeName: result[i]['nodeName']['value'],
                        from: result[i]['from']['value'],
                        iri: result[i]['iri']['value'],
                        joke: result[i]['joke']['value']
                    });
                }

                result = {
                    paramsOk: true,
                    found: true,
                    data: arrOfItems
                };

                fun(result);
            } catch (err) {
                result = {
                    paramsOk: true,
                    found: false,
                    data: []
                };

                fun(result);
            }
        });
};

module.exports = JokeItemDAO;