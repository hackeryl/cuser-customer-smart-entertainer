/**
 * Created by Stefan on 1/22/2016.
 */
var utils = require('../services/randomServiceGenerator');
var check = require('check-types');
var DatabaseManager = require('./stardogDatabaseManager');

function PersonDAO(conn, databaseName) {
    "use strict";

    this.conn = conn;
    this.databaseName = databaseName;
}

/*
 { firstName: string, lastName: string, age: int, mail: string }, function(object)

 { paramsOk: boolean, executed: boolean, data: { id: string } }
 */
PersonDAO.prototype.insert = function (data, fun) {

    var paramsOk = true, executed = true;

    if (!check.string(data.firstName) || check.emptyString(data.firstName)) paramsOk = false;
    //if (!check.string(data.lastName) || check.emptyString(data.lastName))   paramsOk = false;
    if (!check.integer(data.age) || !check.greater(data.age, 0))   paramsOk = false;
    if (!check.string(data.mail) || check.emptyString(data.mail)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            executed: false,
            data: undefined
        });
        return;
    }

    var id = utils.getRandomId();

    var what =
        "  [] rdf:type       foaf:Person;\n" +
        "     ns:id          \"" + id + "\";\n" +
        "     foaf:firstName \"" + data.firstName + "\";\n" +
        "     foaf:lastName  \"" + data.lastName + "\";\n" +
        "     foaf:age         " + data.age + ";\n" +
        "     foaf:mbox      \"" + data.mail + "\"";

    var conditions =
        "   FILTER NOT EXISTS {\n" +
        "      [] rdf:type       foaf:Person;\n" +
        "         foaf:mbox      \"" + data.mail + "\"}";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildInsertWhere(what, conditions)
        },
        function (data) {
            var result = {
                paramsOk: true,
                executed: check.undefined(data['boolean']) ? false : data['boolean'],
                data: {
                    id: check.undefined(data['boolean']) ? undefined :
                        ( data['boolean'] ? id : undefined )
                }
            };

            fun(result);
        });
};

/*
 { mail: string }, function(object)

 { paramsOk: boolean, found: boolean, data: { id: string } }
 */
PersonDAO.prototype.selectIdByEmail = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.string(data.mail) || check.emptyString(data.mail)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            found: false,
            data: undefined
        });
        return;
    }

    var what = "?id";

    var conditions =
        "  ?s rdf:type       foaf:Person;\n" +
        "     foaf:mbox      \"" + data.mail + "\";\n" +
        "     ns:id          ?id\n";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildSelectWhere(what, conditions)
        },
        function (data) {
            var result;

            if (data.results.bindings.length == 0) {
                result = {
                    paramsOk: true,
                    found: false,
                    data: undefined
                };

                console.log("selectIdByEmail: " + JSON.stringify(result));
                fun(result);
                return;
            }
            result = data.results.bindings[0];

            result = {
                paramsOk: true,
                found: true,
                data: {
                    id: result['id']['value']
                }
            };
            console.log("selectIdByEmail: " + JSON.stringify(result));
            fun(result);
        });
};

PersonDAO.prototype.detectClassById = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.string(data.id) || check.emptyString(data.id)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        return {
            paramsOk: false,
            found: false,
            data: undefined
        };
    }

    var what = "?type";

    var conditions =
        "                ?s rdf:type ?type.\n" +
        "                    ?type rdfs:subClassOf* cuser:EntertainmentItem.\n" +
        "                    ?s ns:id \""+data.id+"\"";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildSelectWhere(what, conditions)
        },
        function (data) {
            var result;

            if (data.results.bindings.length == 0) {
                result = {
                    paramsOk: true,
                    found: false,
                    data: undefined
                };

                fun(result);
                return;
            }
            result = data.results.bindings[0];

            result = {
                paramsOk: true,
                found: true,
                data: {
                    type: result['type']['value']
                }
            };

            fun(result);
        });
};

/*
 { id: string }, function(object)

 { paramsOk: boolean, found: boolean, data: { node: string, firstName: string, lastName: string, age: int, mbox: string } }
 */
PersonDAO.prototype.selectById = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.string(data.id) || check.emptyString(data.id)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            found: false,
            data: undefined
        });
        return;
    }

    var what = "?node ?firstName ?lastName ?age ?mbox";

    var conditions =
        "  ?node rdf:type       foaf:Person;\n" +
        "     ns:id          \"" + data.id + "\";\n" +
        "     foaf:firstName ?firstName;\n" +
        "     foaf:lastName  ?lastName;\n" +
        "     foaf:age       ?age;\n" +
        "     foaf:mbox      ?mbox\n";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildSelectWhere(what, conditions)
        },
        function (data) {
            var result;

            if (data.results.bindings.length == 0) {
                result = {
                    paramsOk: true,
                    found: false,
                    data: undefined
                };

                fun(result);
                return;
            }
            result = data.results.bindings[0];

            result = {
                paramsOk: true,
                found: true,
                data: {
                    firstName: result['firstName']['value'],
                    lastName: result['lastName']['value'],
                    age: result['age']['value'],
                    mbox: result['mbox']['value'],
                    node: result['node']['value']
                }
            };

            fun(result);
        });
};

/*
 { id: string, firstName: string, lastName: string, age: int }, function(object)

 { paramsOk: boolean, executed: boolean }
 */
PersonDAO.prototype.updateById = function (data, fun) {
    var paramsOk = true, executed = true;

    var deleteData = "", insertData = "", whereData = "";

    if (!check.string(data.id) || check.emptyString(data.id)) paramsOk = false;
    else {
        whereData =
            "  ?p rdf:type foaf:Person;\n" +
            "     ns:id    \"" + data.id + "\".";
    }
    if (!check.function(fun)) paramsOk = false;

    if (!check.undefined(data.firstName)) {

        if (!check.string(data.firstName) || check.emptyString(data.firstName)) paramsOk = false;
        else {
            deleteData = deleteData.concat("  ?p foaf:firstName ?o1.\n");
            insertData = insertData.concat("  ?p foaf:firstName \"" + data.firstName + "\".\n");
            whereData = whereData.concat("  ?p foaf:firstName ?o1.\n");
        }
    }

    if (!check.undefined(data.lastName)) {

        if (!check.string(data.lastName) || check.emptyString(data.lastName)) paramsOk = false;
        else {
            deleteData = deleteData.concat("  ?p foaf:lastName ?o2.\n");
            insertData = insertData.concat("  ?p foaf:lastName \"" + data.lastName + "\".\n");
            whereData = whereData.concat("  ?p foaf:lastName ?o2.\n");
        }
    }

    if (!check.undefined(data.age)) {

        if (!check.integer(data.age) || !check.greater(data.age, 0)) paramsOk = false;
        else {
            deleteData = deleteData.concat("  ?p foaf:age ?o3.\n");
            insertData = insertData.concat("  ?p foaf:age " + data.age + ".\n");
            whereData = whereData.concat("  ?p foaf:age ?o3.\n");
        }
    }

    if (check.emptyString(deleteData)) {
        deleteData = "nothing";
    }

    if (check.emptyString(insertData)) {
        insertData = "nothing";
    }

    if (!paramsOk) {
        fun({
            paramsOk: false,
            executed: false
        });
        return;
    }

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildDeleteInsertWhere(deleteData, insertData, whereData)
        },
        function (data) {
            var result = {
                paramsOk: true,
                executed: check.undefined(data['boolean']) ? false : data['boolean']
            };

            fun(result);
        });
};

/*
 { id: string, like: string }, function(object)

 { paramsOk: boolean, executed: boolean }
 */
PersonDAO.prototype.insertLike = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.string(data.id) || check.emptyString(data.id)) paramsOk = false;
    if (!check.string(data.like) || check.emptyString(data.like)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            found: false
        });
        return;
    }

    var what = "?s cuser:like \"" + data.like + "\"";

    var conditions =
        "  ?s rdf:type       foaf:Person;\n" +
        "     ns:id          \"" + data.id + "\"";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildInsertWhere(what, conditions)
        },
        function (data) {
            var result = {
                paramsOk: true,
                executed: check.undefined(data['boolean']) ? false : data['boolean']
            };
            fun(result);
        });
};

/*
 { id: string }, function(object)

 { paramsOk: boolean, found: boolean, data: [ { like: string } ] }
 */
PersonDAO.prototype.selectLikesById = function (data, fun) {
    var paramsOk = true, found = true;

    if (!check.string(data.id) || check.emptyString(data.id)) paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            found: false,
            data: undefined
        });
        return;
    }

    var what = "?p";

    var conditions =
        "    ?s rdf:type       foaf:Person;\n" +
        "     ns:id          \"" + data.id + "\";\n" +
        "      cuser:like ?p";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildSelectWhere(what, conditions)
        },
        function (data) {
            var result, i;

            var arrOfLikes = [];

            if (data.results.bindings.length == 0) {
                result = {
                    paramsOk: true,
                    found: false,
                    data: undefined
                };

                fun(result);
                return;
            }

            result = data.results.bindings;

            for (i = 0; i < result.length; i++) {

                arrOfLikes.push({
                    like: result[i]['p']['value']
                });
            }

            result = {
                paramsOk: true,
                found: true,
                data: arrOfLikes
            };

            fun(result);
        });
};

/*
 { personId1: string, personId2: string }, function(object)

 { paramsOk: boolean, executed: boolean}
 */
PersonDAO.prototype.addFriends = function (data, fun) {

    var paramsOk = true, executed = true;

    if (!check.string(data.personId1) || check.emptyString(data.personId1)) paramsOk = false;
    if (!check.string(data.personId2) || check.emptyString(data.personId2))   paramsOk = false;
    if (!check.function(fun)) paramsOk = false;

    if (!paramsOk) {
        fun({
            paramsOk: false,
            executed: false
        });
        return;
    }

    var what =
        "  ?s1 foaf:knows ?s2.\n" +
        "  ?s2 foaf:knows ?s1";

    var conditions =
        "  ?s1 rdf:type foaf:Person;\n" +
        "      ns:id \"" + data.personId1 + "\".\n" +
        "  ?s2 rdf:type foaf:Person;\n" +
        "      ns:id \"" + data.personId2 + "\".";

    this.conn.query({
            database: this.databaseName,
            query: DatabaseManager.buildInsertWhere(what, conditions)
        },
        function (data) {
            var result = {
                paramsOk: true,
                executed: check.undefined(data['boolean']) ? false : data['boolean']
            };
            console.log("addFriends: " + result);
            fun(result);
        });
};

/*
 data: { node: string, id: string, firstName: string, mbox: string }

 rdf: string
 */
PersonDAO.prototype.constructRDF = function (data, fun) {

    fun("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
    "<rdf:RDF\n" +
    "\txmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
    "\txmlns:owl=\"http://www.w3.org/2002/07/owl#\"\n" +
    "\txmlns:xsd=\"http://www.w3.org/2001/XMLSchema#\"\n" +
    "\txmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\">\n" +
    "\n" +
    "<rdf:Description rdf:nodeID=\""+data.node+"\">\n" +
    "\t<rdf:type rdf:resource=\"http://xmlns.com/foaf/0.1/#Person\"/>\n" +
    "\t<id xmlns=\"http://rdfs.org/sioc/ns#\" rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">"+data.id+"</id>\n" +
    "\t<firstName xmlns=\"http://xmlns.com/foaf/0.1/#\" rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">"+data.firstName+"</firstName>\n" +
    "\t<mbox xmlns=\"http://xmlns.com/foaf/0.1/#\" rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">"+data.mbox+"</mbox>\n" +
    "</rdf:Description>\n" +
    "\n" +
    "</rdf:RDF>");
};

module.exports = PersonDAO;