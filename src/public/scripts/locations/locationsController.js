/**
 * Created by Vasile on 24-Jan-16.
 */
var app = angular.module("Locations");

app.controller("LocationsController", ["$scope", "$window", "$log", "locationsService", LocationsController]);

function LocationsController($scope, $window, $log, locationsService) {
    var ctrl = this;

    ctrl.UserGeolocation = {};
    ctrl.SelectedLocation = "";
    ctrl.Locations = [];

    var getNearbyLocations = function (position) {
        var crd = position.coords;
        ctrl.UserGeolocation = crd;

        locationsService.getNearbyLocations(crd.latitude, crd.longitude, function (locations) {
            $scope.$apply(function () {
                ctrl.Locations = locations;
            });
        });
    };

    var getGeolocation = function (success) {
        var options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };

        function error(err) {
            $window.alert('ERROR(' + err.code + '): ' + err.message);
        };

        navigator.geolocation.getCurrentPosition(success, error, options);
    };

    ctrl.selectLocation = function (location) {
        ctrl.SelectedLocation = location;
    };

    ctrl.save = function () {
        if(ctrl.UserGeolocation && ctrl.SelectedLocation){
            locationsService.saveUserLocation(ctrl.UserGeolocation, ctrl.SelectedLocation).then(function (response) {
                if (response.success) {
                    $window.location.href = "/products"
                }
                else {
                    $window.alert(response.message);
                }
            });
        }
        else{
            if(!ctrl.UserGeolocation){
                $window.alert("Please allow the application to get your coordinates!");
            }
            else{
                $window.alert("Please select or insert your current location!");
            }
        }
    };

    $window.onload = function () {
        getGeolocation(getNearbyLocations);
    };

    return ctrl;
};