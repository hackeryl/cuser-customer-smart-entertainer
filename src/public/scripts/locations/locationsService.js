/**
 * Created by Vasile on 24-Jan-16.
 */
var app = angular.module("Locations");

app.value("google", google);
app.factory("locationsService", ["$http", "google", LocationsService]);

function LocationsService($http, google) {
    var service = this;

    service.getNearbyLocations = function (latitude, longitude, callback) {
        var location = new google.maps.LatLng(latitude, longitude);

        var request = {
            location: location,
            radius: '50'
        };

        var placesService = new google.maps.places.PlacesService(document.createElement('div'));
        placesService.nearbySearch(request, function (results, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                callback(results);
            }
            else {
                callback([]);
            }
        });
    };

    service.getNearbyLocationsOld = function (latitude, longitude) {
        return $http.get("https://maps.googleapis.com/maps/api/place/nearbysearch/json", {
            params: {
                key: "AIzaSyCnsfLqmRLR3vTugzSNmqz8DEwB69RBlPA",
                radius: 50,
                location: latitude + "," + longitude
            }
        }).then(function (response) {
            if (response.status === "OK") {
                return response.results;
            } else {
                return [];
            }
        });
    };

    service.saveUserLocation = function (userGeolocation, pointOfInterest) {
        return $http.post("/users/update/location", {
            userLocation: {
                latitude: userGeolocation.latitude,
                longitude: userGeolocation.longitude,
                pointOfInterest: pointOfInterest
            }
        }).then(function (response) {
            return response.data;
        });
    };

    return this;
};
