var app = angular.module("Products");

app.controller("ProductsController", ["$scope", "$window", "$log", "productsService", ProductsController]);

function ProductsController($scope, $window, $log, productsService) {
    var ctrl = this;

    ctrl.PreparingTime = undefined;
    ctrl.SelectedProduct = "";
    ctrl.Products = [];

    var getProducts = function () {
        productsService.getProducts(function (products) {
                ctrl.Products = products;
        });
    };

    ctrl.selectProduct = function (product) {
        ctrl.SelectedProduct = product;
    };

    ctrl.save = function () {
        if (ctrl.SelectedProduct && ctrl.PreparingTime) {
            productsService.saveProduct(ctrl.SelectedProduct, ctrl.PreparingTime).then(function (response) {
                if (response.success) {
                    $window.location.href = "/"
                }
                else {
                    $window.alert(response.message);
                }
            });
        }
        else {
            if (!ctrl.SelectedProduct) {
                $window.alert("Please select or insert your ordered product!");
            }
            else {
                $window.alert("Please insert the product preparing time!");
            }
        }
    };

    $window.onload = function () {
        getProducts();
    };

    return ctrl;
};