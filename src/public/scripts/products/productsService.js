var app = angular.module("Products");

app.factory("productsService", ["$http", ProductsService]);

function ProductsService($http) {
    var TYPES = ['BREAKFAST', 'BRUNCH', 'LUNCH', 'DINNER'];
    var HOURS = [[0, 10], [10, 13], [13, 17], [17, 24]];

    var service = this;

    service.getProducts = function (callback) {
        var FOOD_LIST = [];
        var LOADED = 0;
        var d = new Date();
        var hour_now = d.getHours();
        var TYPE = '';
        for (var i = 0; i < TYPES.length; i++) {
            if (HOURS[i][0] < hour_now && HOURS[i][1] >= hour_now)
                TYPE = TYPES[i];
        }

        var getDefaultProducts = function(){
            var products = [];

            var pizza = {};
            pizza.name = "Pizza";
            pizza.description = "Pizza";

            var juice = {};
            juice.name = "Juice";
            juice.description = "Juice";

            var coffee = {};
            coffee.name = "Coffee";
            coffee.description = "coffee";

            var burger = {};
            burger.name = "Burger";
            burger.description = "burger";

            products.push(pizza);
            products.push(juice);
            products.push(coffee);
            products.push(burger);
            return products;
        };

        $http.get("http://api.cs50.net/food/3/menus", {
            params: {
                key: "33c170cb19704291af91f38fccc607f7",
                meal: TYPE,
                output: "json"
            }
        }).then(function (response) {
            for (var j = 0; j < response.data.length; j++) {
                var food = {};
                food.name = response.data[j].name;
                food.description = response.data[j].category;
                FOOD_LIST.push(food);
            }
            if(FOOD_LIST.length === 0){
                FOOD_LIST = getDefaultProducts();
            }
            callback(FOOD_LIST);

        });

    };

    service.saveProduct = function (productName, preparingTime) {
        return $http.post("/users/update/product", {
            product: {
                name: productName,
                preparingTime: preparingTime
            }
        }).then(function (response) {
            return response.data;
        });
    };

    return this;
};

