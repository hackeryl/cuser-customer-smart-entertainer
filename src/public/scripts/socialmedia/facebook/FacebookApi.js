/**
 * Created by Vasile on 15-Dec-15.
 */
function FacebookApi() {
    this.init();
};

FacebookApi.prototype.init = function () {
    FB.init({
        appId: '1666594170277111',
        cookie: true,  // enable cookies to allow the server to access the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.2' // use version 2.2
    });
};

FacebookApi.prototype.login = function (callback) {
    FB.getLoginStatus(function (response) {
        if (response.status === 'connected') {
            console.log('Logged in with facebook.');
            callback({Success: true});
        }
        else {
            FB.login(function (response) {
                if (response.authResponse) {
                    callback({Success: true});
                }
                else {
                    callback({Success: false});
                }
            }, {scope: 'public_profile,email,user_likes,user_friends'});
        }
    });
};

FacebookApi.prototype.logout = function () {
    FB.logout(function (response) {
        console.log('Logged out from facebook.');
    });
};

FacebookApi.prototype.getFriendsList = function (callback) {
    var friends = [];
    FB.api("/me/friends?fields=picture", function (response) {
        for (var i = 0; i < response.data.length; i++) {
            var friend = new UserProfile("Facebook", response.data[i].id, response.data[i].name, response.data[i].email,
                response.data[i].picture.data.url, null);
            friends.push(friend);
        }

        callback(friends);
    });
};

FacebookApi.prototype.getFavourites = function (callback) {
    var favourites = [];
    FB.api("/me/likes", function (response) {
        for (var i = 0; i < response.data.length; i++) {
            favourites.push(response.data[i].name);
        }

        callback(favourites);
    });
};

FacebookApi.prototype.getUserProfile = function (callback) {
    FB.api('/me?fields=name,email,picture', function (user) {
        var email = user.id + '@facebook.com'
        var userProfile = new UserProfile("Facebook", user.id, user.name, email, user.picture.data.url, null);
        callback(userProfile);
    });
};