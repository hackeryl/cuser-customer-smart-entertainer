/**
 * Created by devWind01 on 12/15/2015.
 */
function GooglePlusAPI() {
    this.ImageUrl = "";
    this.init = function () {
        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = false;
        po.src = 'https://apis.google.com/js/client.js?onload=onLoadCallback';
        var s = document.getElementsByTagName('head')[0];
        s.parentNode.insertBefore(po, s);
    };
}
var googleApi = null;

GooglePlusAPI.prototype.login = function () {
    var myParams = {
        'clientid': '43158932005-vj3ihqe2041imh772jfe4f52no4b4h91.apps.googleusercontent.com',
        'cookiepolicy': 'single_host_origin',
        'callback': 'loginCallback',
        'approvalprompt': 'force',
        'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.me' +
        ' https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'
    };
    gapi.auth.signIn(myParams);
};

function loginCallback(result) {
    if (result['status']['signed_in']) {
        googleApi.getUserProfile(function (resp1) {
            var _user = processMe(resp1);
            googleApi.getFriendsList(function (resp2) {
                console.log(resp2);
                processFriendsList(resp2, function (friendList) {
                    var user = {};
                    var i;
                    user.socialPlatform = 'Google Plus';
                    user.userId = _user.userId;
                    user.username = _user.username;
                    user.email = _user.userId + '@gmail.com';
                    user.ImageUrl = _user.imageUrl;
                    user.token = _user.token;
                    user.likes = [];
                    user.friends = [];
                    for (i = 0; i < friendList.length; i++) {
                        if(friendList[i]) {
                            user.friends.push(friendList[i]);
                        }
                    }
                    updateUserInfo(user);
                });
            });
        });
    }
}

GooglePlusAPI.prototype.logout = function () {

    gapi.auth.signOut();
    location.reload();
};

GooglePlusAPI.prototype.getFriendsList = function (callback) {

    var request = gapi.client.plus.people.list({
        'userId': 'me',
        'collection': 'visible'
    });
    return request.execute(callback);
};

GooglePlusAPI.prototype.getFavourites = function () {

    var request = gapi.client.plus.activities.search({
        'query': 'Awesome'
    });

    request.execute(function (resp) {
        var numItems = resp.items.length;
        for (var i = 0; i < numItems; i++) {
            console.log('ID: ' + resp.items[i].id + ' Content: ' +
                resp.items[i].object.content);
        }
    });
};

GooglePlusAPI.prototype.getUserProfile = function (callback) {

    var request = gapi.client.plus.people.get({
        'userId': 'me'
    });
    return request.execute(callback);

};

function onLoadCallback() {
    gapi.client.setApiKey('AIzaSyDhTk3re8l7vgAwEz-v9y2iZN6y4qYuK6w');
    gapi.client.load('plus', 'v1').then(function () {
        googleApi = new GooglePlusAPI();
        googleApi.login();
        console.log('loaded.');

    });
}
function processMe(resp) {
    if ('error' in resp) {

        return undefined;
    }
    var id = resp.id;
    var displayName = resp.displayName;
    var email = resp['emails'].length > 0 ? resp['emails'][0].value : undefined;
    var imageUrl = resp.image.url;
    var accesToken = gapi.auth.getToken()['access_token'];
    var userProfile = new UserProfile("Google Plus", id, displayName, email, imageUrl, accesToken);
    return userProfile;
}
function processFriend(resp) {
    console.log(resp);
    if ('error' in resp) {
        return undefined;
    }
    var id = resp.id;
    var displayName = resp.displayName;
    var imageUrl = resp.image.url;
    var email = resp.id + '@gmail.com';
    var userProfile = new UserProfile("Google Plus", id, displayName, email, imageUrl);
    return userProfile;
}
var FRIENDS = [];
function processFriendsList(resp, callback) {
    if ('error' in resp) {
        callback([]);
        return undefined;
    }

    FRIENDS = [];
    var numItems = resp.items.length;
    for (var i = 0; i < numItems; i++) {
        var id = resp.items[i].id;
        var request = gapi.client.plus.people.get({
            'userId': id
        });
        request.execute(function (response) {
            var friend = processFriend(response);
            FRIENDS.push(friend);
            if (FRIENDS.length === numItems) {
                callback(FRIENDS);
            }
        });
    }
}


var updateUserInfo = function (user) {
    $.ajax({
        url: "/users/update",
        type: "POST",
        dataType: "json",
        data: {user: JSON.stringify(user)},
        success: function (response) {
            if (response.success) {
                window.location.href = "/locations";
            }
            else {
                window.location.href = "/error";
            }
        }
    });
};