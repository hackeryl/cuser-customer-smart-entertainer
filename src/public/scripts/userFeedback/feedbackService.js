var app = angular.module("Entertainment");

app.factory("feedbackService", ["$http", FeedbackService]);

function FeedbackService($http) {
    var service = this;

    service.saveFeedback = function (id, rating, comment) {
        return $http.post("/feedbacks",
            {
                id: id,
                rating: rating,
                review: comment
            }
        ).then(function (response) {
            return response.data;
        });
    };
    service.getFeedbacksId = function (id) {
        return $http.get("/feedbacks/entertainmentItem/" + id
        ).then(function (response) {
            return response.data;

        });
    };
    service.getFeedbacks = function (id) {

            return $http.get("/feedbacks/" + id
            ).then(function (response) {
               // console.log(response);
                return response.data;

            });

    };

    return this;
};

