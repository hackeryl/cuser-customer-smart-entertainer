var app = angular.module("Entertainment");

app.controller("FeedbackController", ["$scope", "$window", "$log", "feedbackService", FeedbackController]);

function FeedbackController($scope, $window, $log, feedbackService) {
    var ctrl = this;

    ctrl.rating = 1;
    ctrl.comment = "";
    ctrl.id = 0;
    ctrl.Feedbacks = [];

    ctrl.save = function () {

        feedbackService.saveFeedback(ctrl.id, ctrl.rating, ctrl.comment).then(function (response) {
            if (response.success) {
                // $window.location.href = "/"
                $window.location.reload();
            }
            else {
                $window.alert(response.message);
            }
        });
    };

    var init = function (entertainmentItemId) {
        ctrl.id = entertainmentItemId;
        feedbackService.getFeedbacksId(ctrl.id).then(function (response) {
            if (response.success) {
                for (var i = 0; i < response.data.length; i++) {
                    feedbackService.getFeedbacks(response.data[i].id).then(function (res) {
                        // ctrl.Feedbacks = response.data;
                        ctrl.Feedbacks.push(res.data);
                        console.log(res.data);

                    });
                }
            }
            else {
                console.log(response);
            }

        });
    };

    ctrl.init = init;


    return ctrl;
};