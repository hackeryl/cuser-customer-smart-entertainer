/**
 * Created by Vasile on 24-Jan-16.
 */
function UserProfile(socialPlatform, userId, userName, email, imageUrl, token) {
    this.socialPlatform = socialPlatform;
    this.userId = userId;
    this.username = userName;
    this.email = email;
    this.imageUrl = imageUrl;
    this.token = token;
    this.friends = [];
    this.likes = [];
};