/**
 * Created by Vasile on 29-Jan-16.
 */
var app = angular.module("Entertainment");

app.controller("EntertainmentItemController", ["$scope", "$window", "$log", "entertainmentService", EntertainmentItemController]);

function EntertainmentItemController($scope, $window, $log, entertainmentService) {
    var ctrl = this;

    ctrl.item = {};

    var getEntertainmentItem = function (entertainmentItemId) {
        entertainmentService.getEntertainmentItem(entertainmentItemId).then(function (item) {
            ctrl.item = item;
            $log.info(ctrl.item);
        });
    };

    var init = function (entertainmentItemId) {
        getEntertainmentItem(entertainmentItemId);
    };

    ctrl.init = init;

    return ctrl;
};