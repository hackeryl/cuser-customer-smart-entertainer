/**
 * Created by Vasile on 24-Jan-16.
 */
var app = angular.module("Entertainment");

app.factory("entertainmentService", ["$http", EntertainmentService]);

function EntertainmentService($http) {
    var service = this;

    service.getEntertainmentItems = function (offset, limit) {
        return $http.get("/entertainment", {
            params: {
                offset: offset,
                limit: limit
            }
        }).then(function (response) {
            return response.data.data;
        });
    };

    service.getEntertainmentItem = function (id) {
        return $http.get("/entertainment/" + id).then(function (response) {
            return response.data.data;
        });
    };

    return this;
};

