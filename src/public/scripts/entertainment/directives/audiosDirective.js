/**
 * Created by Vasile on 29-Jan-16.
 */
var app = angular.module("Entertainment");

app.directive('audios', function($sce) {
    return {
        restrict: 'A',
        scope: { code:'=' },
        replace: true,
        template: '<audio ng-src="{{url}}" controls></audio>',
        link: function (scope) {
            scope.$watch('code', function (newVal, oldVal) {
                if (newVal !== undefined) {
                    scope.url = $sce.trustAsResourceUrl(newVal);
                }
            });
        }
    };
});