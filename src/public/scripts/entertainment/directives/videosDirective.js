/**
 * Created by Vasile on 29-Jan-16.
 */
var app = angular.module("Entertainment");

app.directive('videos', function($sce) {
    return {
        restrict: 'A',
        scope: { code:'=' },
        replace: true,
        template: '<iframe ng-src="{{url}}" controls></iframe>',
        link: function (scope) {
            scope.$watch('code', function (newVal, oldVal) {
                if (newVal !== undefined) {
                    scope.url = $sce.trustAsResourceUrl(newVal);
                }
            });
        }
    };
});