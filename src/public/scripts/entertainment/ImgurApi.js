/**
 * Created by Vasile on 24-Jan-16.
 */
function ImgurApi() {
    this.url = "https://api.imgur.com/3/gallery/search/";
};

ImgurApi.prototype.getEntertainmentItems = function (keyword, callback) {
    $.ajax({
        url: this.url,
        data: {q_any: keyword},
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Client-ID 029da0d0e149d19');
        },
        success: function (response) {
            if (response.success) {
                callback(response.data);
            } else {
                callback(null);
            }
        }
    });
};