/**
 * Created by Vasile on 24-Jan-16.
 */
var app = angular.module("Entertainment");

app.controller("EntertainmentController", ["$scope", "$window", "$log", "entertainmentService", EntertainmentController]);

function EntertainmentController($scope, $window, $log, entertainmentService) {
    var ctrl = this;

    ctrl.EntertainmentItems = [];
    ctrl.Offset = 0;
    ctrl.Limit = 10;

    var getEntertainmentItems = function () {
        entertainmentService.getEntertainmentItems(ctrl.Offset, ctrl.Limit).then(function(items){
            ctrl.EntertainmentItems = items;
        });
    };

    var init = function () {
        getEntertainmentItems();
    };

    ctrl.init = init;

    ctrl.init();

    return ctrl;
};