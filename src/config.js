/**
 * Created by Vasile on 24-Jan-16.
 */
var config = {};
config.twitter = {};
config.twitter.consumerKey = "VOdXO3OtaDW4cxk8lJsgs3t6j";
config.twitter.consumerSecret = "jFLOxU8hU0IYUlZQ0agXE4qYfHUiXqCYRSGI7TgGjMEv4LM2HO";
config.twitter.callback = "http://127.0.0.1:3000/authentication/twitter/callback";

config.youtube = {};
config.youtube.key = "AIzaSyAyhyLABTpjQOhWoHtUJoA9tT85nYc2sSM";
config.youtube.url = "https://www.youtube.com/watch?v=";
config.youtube.src = "https://www.youtube.com/embed/";

config.googleAudio = {};
config.googleAudio.url = "https://translate.google.com/translate_tts?ie=UTF-8&tl=en&client=tw-ob&q=";

config.icndb = {};
config.icndb.url = "http://api.icndb.com/jokes/random/";
config.icndb.src = "http://api.icndb.com/joke/";
config.stardog = {
    url: "http://localhost:5820/",
    username: "admin",
    password: "admin",
    databaseName: "CuSEr"
};

config.cuserClasses = {
    cuser: "http://www.semanticweb.org/stefan/ontologies/2016/0/CuSEr#",
    prefix: "cuser",

    person: "foaf:Person",
    pointOfInterest: "dbo:place",
    product: "gr:ProductOrServiceModel",
    userFeedback: "cuser:UserFeedback",

    entertainmentItem: "cuser:EntertainmentItem",
    audioItem: "cuser:AudioItem",
    videoItem: "cuser:VideoItem",
    jokeItem: "cuser:JokeItem",
    imageItem: "cuser:ImageItem",

    api: "cuser:API",
    youtubeAPI: "cuser:YoutubeAPI",
    nineGagAPI: "cuser:9GagAPI",
    icndbAPI: "cuser:IcndbAPI",
    googleAudioAPI: "cuser:GoogleAudioAPI",

    location: "geo:Point",
    time: "time:DurationDescription"
};

module.exports = config;