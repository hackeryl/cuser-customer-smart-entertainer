# CuSEr (Customer Smart Entertainer) #

A restaurant (coffee shop, pub,...) wants to create a new smart social experience for its customers by making use of different social networks and semantic Web technologies. Based on specific or general conceptual models (e.g., DBpedia), create such an experience which offers customers – in an intelligent manner (by using user feedback, reasoning, and/or machine learning) – various entertainment facilities like comics, funny videos, electronic/real games or jokes while waiting for drinks/meals. Also, several collaborative interaction games with other people (i.e. those having similar gastronomical tastes) from the same food establishment may be suggested. The solution will also address specific privacy issues. Bonus: providing a multi-language user experience.

Link for State of the art: [State of the art](https://docs.google.com/document/d/1ESSfzAx-RvEoEGzPHZI2rPSBnBeaaqltgY-EHe2NHAE/edit)

Requirement Analysis: [Requirement Analysis](https://docs.google.com/document/d/1StqSMWA4BflWwdawyNhx1vSSmE6JOzOXnXmyYoggUNE/edit?usp=sharing)

UML Diagrams: [UML Diagrams](https://docs.google.com/document/d/1TCTb39uT_mUG3VfmCzrpsGuYJg4ED0x-9yTqzKRvH9w/edit)

Design patterns: [Design patterns](https://docs.google.com/document/d/1hX3G6ciABdcsSWSLFh7TqfwtQHY20J08tZNsEXN_3u0/edit)

BPMN Diagram: [BPMN Diagram](https://docs.google.com/document/d/1AF5w5EZEBU6R2DOGaY_UgAsr1cT7Z4Lr0ItGIDxzQHk/edit?usp=sharing)

Tehnical Report: [Tehnical Report](https://docs.google.com/document/d/1mCjJEf_RillJx_aCaOzUYeYz3jW0xsDU97jtViazBOY/edit)

Comparison with other methods: [Comparison With Other Methods](https://docs.google.com/document/d/1njB7SScN_BWWf0dh6SsJT2kH8vZlT0UlRv64i_zaDN8/edit)

SOA: [SOA](https://drive.google.com/file/d/0Bx-mj7rbnBjkcFlvcmhLd0NSbFk/view)

Blog: [cuserapp.wordpress.com](https://cuserapp.wordpress.com/)