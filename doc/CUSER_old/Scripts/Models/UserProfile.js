/**
 * Created by Vasile on 15-Dec-15.
 */
function UserProfile(socialPlatform, userId, userName, email, imageUrl, token) {
    this.SocialPlatform = socialPlatform;
    this.UserId = userId;
    this.Username = userName;
    this.Email = email;
    this.ImageUrl = imageUrl;
    this.Token = token;
};