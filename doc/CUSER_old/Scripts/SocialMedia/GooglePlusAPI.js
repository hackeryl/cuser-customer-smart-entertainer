/**
 * Created by devWind01 on 12/15/2015.
 */

function GooglePlusAPI() {

    this.ImageUrl = "";
    this.init = function () {

        var po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = false;
        po.src = 'https://apis.google.com/js/client.js?onload=onLoadCallback';
        var s = document.getElementsByTagName('head')[0];
        s.parentNode.insertBefore(po, s);
    };
}

GooglePlusAPI.prototype.login = function () {

    var myParams = {
        'clientid': '43158932005-vj3ihqe2041imh772jfe4f52no4b4h91.apps.googleusercontent.com',
        'cookiepolicy': 'single_host_origin',
        'callback': 'loginCallback',
        'approvalprompt': 'force',
        'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.me' +
        ' https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'
    };
    gapi.auth.signIn(myParams);
};

function loginCallback(result) {

    if (result['status']['signed_in']) {

        var request = gapi.client.plus.people.get({
            'userId': 'me'
        });

        return request.execute(processMe);
    }
}

GooglePlusAPI.prototype.logout = function () {

    gapi.auth.signOut();
    location.reload();
};

GooglePlusAPI.prototype.getFriendsList = function () {

    var request = gapi.client.plus.people.list({
        'userId': 'me',
        'collection': 'visible'
    });

    request.execute(processFriendsList);
};

GooglePlusAPI.prototype.getFavourites = function () {

    var request = gapi.client.plus.activities.search({
        'query' : 'Awesome'
    });

    request.execute(function(resp) {
        var numItems = resp.items.length;
        for (var i = 0; i < numItems; i++) {
            console.log('ID: ' + resp.items[i].id + ' Content: ' +
                resp.items[i].object.content);
        }
    });
};

GooglePlusAPI.prototype.getUserProfile = function () {

    var request = gapi.client.plus.people.get({
        'userId': 'me'
    });

    return request.execute(processMe);
};

function onLoadCallback() {

    gapi.client.setApiKey('AIzaSyDhTk3re8l7vgAwEz-v9y2iZN6y4qYuK6w');
    gapi.client.load('plus', 'v1').then(function () {
        console.log('loaded.');
    });
}

function processMe(resp) {

    //console.log(resp['ageRange']['min']);

    //console.log(resp['displayName']);
    //console.log(resp['name']['familyName']);
    //console.log(resp['name']['givenName']);

    //console.log(resp['gender']);

    if ('error' in resp) {

        return undefined;
    }

    var id = resp.id;
    var displayName = resp.displayName;
    var email = resp['emails'].length > 0 ? resp['emails'][0].value : undefined;
    var imageUrl = resp.image.url;
    var accesToken = gapi.auth.getToken()['access_token'];

    var userProfile = new UserProfile("Google Plus", id, undefined, displayName, email, imageUrl, accesToken);

    console.log("me:");
    console.log(userProfile);

    return userProfile;
}
function processFriend(resp) {

    //console.log(resp['displayName']);
    //console.log(resp['name']['familyName']);
    //console.log(resp['name']['givenName']);

    //console.log(resp['gender']);

    if ('error' in resp) {

        return undefined;
    }

    var id = resp.id;
    var displayName = resp.displayName;
    var imageUrl = resp.image.url;

    var userProfile = new UserProfile("Google Plus", id, undefined, displayName, undefined, imageUrl, undefined);

    console.log("process me");
    console.log(userProfile);

    return userProfile;
}
function processFriendsList(resp) {

    if ('error' in resp) {

        return undefined;
    }

    var friends = [];

    var numItems = resp.items.length;
    for (var i = 0; i < numItems; i++) {

        var id = resp.items[i].id;

        var request = gapi.client.plus.people.get({
            'userId': id
        });

        var friend = request.execute(processFriend);

        friends.push(friend);
    }

    return friends;
}