/**
 * Created by stefanstan on 18/11/15.
 */

var app = require('./helpers/app');

var should = require('chai').should(),
    expect = require('chai').expect,
    supertest = require('supertest'),
    api = supertest('localhost:3000/api');

describe('User', function () {

    it('should create a user', function (done) {
        api.get('/items')
            .set('Content-type', 'application/x-www-form-urlencoded')
            .set('Authorization', 'Basic U3RlZmFuOnBhcm9sYQ==')
            .expect(function(res) {
                return res.body == [ { _id: '5655709285b8ff283a6f4adc',
                    userId: '5655708e85b8ff283a6f4adb',
                    quantity: 3,
                    type: 'Joke',
                    name: 'Some Item',
                    __v: 0 } ]
            }, true)
            .expect(200, done);
    })
});