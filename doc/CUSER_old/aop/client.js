/**
 * Created by stefanstan on 25/11/15.
 */

var Client = require('../controllers/client');

var postClients = Client.postClients;

Client.postClients = function(req, res) {
    try {
        return postClients(req, res);
    } catch (e){

        console.log(e);
        throw e;
    }
};