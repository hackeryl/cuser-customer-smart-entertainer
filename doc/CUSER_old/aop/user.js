var User = require('../controllers/user');

var postUsers = User.postUsers;
var getUsers = User.getUsers;

User.postUsers = function(req, res) {
    try {
        return postUsers(req, res);
    } catch (e){
        console.log(e);
        throw e;
    }
};

User.getUsers = function(req, res) {
    try {
        return getUsers(req, res);
    } catch (e){
        console.log(e);
        throw e;
    }
};