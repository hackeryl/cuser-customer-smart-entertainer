var Item = require('../controllers/item');

var postItems = Item.postItems;
var getItems = Item.getItems;
var putItem = Item.putItem;
var getItems = Item.getItem;
var deleteItem = Item.deleteItem;

Item.postItems = function(req, res) {
    try {
        return postItems(req, res);
    } catch (e){
        console.log(e);
        throw e;
    }
};

Item.getItems = function(req, res) {
    try {
        return getItems(req, res);
    } catch (e){
        console.log(e);
        throw e;
    }
};

Item.putItem = function(req, res) {
    try {
        return putItem(req, res);
    } catch (e){
        console.log(e);
        throw e;
    }
};

Item.getItem = function(req, res) {
    try {
        return getItem(req, res);
    } catch (e){
        console.log(e);
        throw e;
    }
};

Item.deleteItem = function(req, res) {
    try {
        return deleteItem(req, res);
    } catch (e){
        console.log(e);
        throw e;
    }
};