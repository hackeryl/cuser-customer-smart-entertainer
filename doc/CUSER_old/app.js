/**
 * Created by stefanstan on 18/11/15.
 */

module.exports = function(databaseConnURI) {

    // Load required packages
    var express = require('express');
    var mongoose = require('mongoose');
    var bodyParser = require('body-parser');
    var ejs = require('ejs');
    var session = require('express-session');
    var passport = require('passport');
    var itemController = require('./controllers/item');
    var userController = require('./controllers/user');
    var authController = require('./controllers/auth');
    var oauth2Controller = require('./controllers/oauth2');
    var clientController = require('./controllers/client');

    // Connect to the itemlocker MongoDB
    //mongoose.connect('mongodb://localhost:27017/itemlocker');

    // Create our Express application
    var app = express();

    // all environments
    app.set('port', process.env.PORT || 3000);

    app.use(function (req, res, next) {
        res.set('X-Powered-By', 'CuSEr');
        next();
    });

    // Set view engine to ejs
    app.set('view engine', 'ejs');

    // Use the body-parser package in our application
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    // Use express session support since OAuth2orize requires it
    app.use(session({
        secret: 'Super Secret Session Key',
        saveUninitialized: true,
        resave: true
    }));

    // Use the passport package in our application
    app.use(passport.initialize());

    app.use('/Scripts/Models/UserProfile.js', express.static(__dirname + '/Scripts/Models/UserProfile.js'));
    app.use('/Scripts/SocialMedia/FacebookApi.js', express.static(__dirname + '/Scripts/SocialMedia/FacebookApi.js'));
    app.use('/Scripts/SocialMedia/GooglePlusApi.js', express.static(__dirname + '/Scripts/SocialMedia/GooglePlusApi.js'));
    app.use('/Scripts/SocialMedia/TwitterApi.js', express.static(__dirname + '/Scripts/SocialMedia/TwitterApi.js'));
    app.use('/Assets/Images/facebook.png', express.static(__dirname + '/Assets/Images/facebook.png'));
    app.use('/Assets/Images/google.png', express.static(__dirname + '/Assets/Images/google.png'));
    app.use('/Assets/Images/twitter.png', express.static(__dirname + '/Assets/Images/twitter.png'));



    //**********twitter****************************/****************************
    var	Twitter = require("node-twitter-api");
    var twitter = new Twitter({
        consumerKey: "VOdXO3OtaDW4cxk8lJsgs3t6j",
        consumerSecret: "jFLOxU8hU0IYUlZQ0agXE4qYfHUiXqCYRSGI7TgGjMEv4LM2HO",
        callback: "http://127.0.0.1:3000/twitter"
    });

    var _requestSecret;
    app.get("/_twitter", function(req, res) {
        twitter.getRequestToken(function(err, requestToken, requestSecret) {
            if (err)
            {
                res.status(500).send(err);

            }
            else {
                _requestSecret = requestSecret;
                console.log(_requestSecret);
                res.redirect("https://api.twitter.com/oauth/authenticate?oauth_token=" + requestToken);
            }
        });
    });

    app.get("/twitter", function(req, res) {
        var requestToken = req.query.oauth_token,
            verifier = req.query.oauth_verifier;
        twitter.getAccessToken(requestToken, _requestSecret, verifier, function(err, accessToken, accessSecret) {
            if (err)
                res.status(500).send(err);
            else
                twitter.verifyCredentials(accessToken, accessSecret, function(err, user) {
                    if (err)
                        res.status(500).send(err);
                    else {
                        sess = req.session;
                        user.accessToken=accessToken;
                        user.accessSecret=accessSecret;
                        sess.user=user;
                        res.render('twitter.ejs', {user: user});
                }
                });
        });
    });


     app.get("/getFavourites", function(req, res) {
      sess = req.session;
      user = sess.user;
          twitter.favorites("list", 
                        { count:"2",
                        screen_name: user.screen_name,
                        },
                    user.accessToken,
                    user.accessSecret,
                    function(error,data,response){
                        if(error){
                            console.log('error');
                        }
                        else
                        {
                            console.log(data);
                            //res.render('friendsList.ejs', {data: data});
                                                    }
                    }                
                    );
        console.log('getFavourites');     
    });
     app.get("/getFriendsList", function(req, res) {
      sess = req.session;
      user = sess.user;
          twitter.friends("list", 
                        { cursor:"-1",
                        screen_name: user.screen_name,
                        skip_status:"true",
                        include_user_entities:"false"

                                        },
                    user.accessToken,
                    user.accessSecret,
                    function(error,data,response){
                        if(error){
                            console.log('error');
                        }
                        else
                        {
                            //console.log(data);
                             res.render('friendsList.ejs', {data: data});
                                                    }
                    }                
                    );
        console.log('getFriendsList');     
    });
    //********************************************************************

///**********9gag*******


//var gagScraper = require('9gag-scraper');
var gag = require('node-9gag')

app.get("/gag", function(req, res) {
var s=req.query.query;
gag.find(s, function (err, data) {
 res.render('gag.ejs', {data: data.result});
 console.log(data);
});
});

app.get("/FavouritesGags", function(req, res) {
     sess = req.session;
      user = sess.user;
          twitter.favorites("list", 
                        { count:"2",
                        screen_name: user.screen_name,
                        },
                    user.accessToken,
                    user.accessSecret,
                    function(error,data,response){
                        if(error){
                            console.log('error');
                        }
                        else
                        {  i=0;
                            var results = [];
                            while (i < data.length) {
                                console.log(data[i].user.screen_name);
                                var s = data[i].user.screen_name;
                                gag.find(s, function (err, data) {
                                     j=0;
                                    while(j<data.result.length)
                                    {
                                     results.push(data.result);
                                     j++;
                                    }
                              });
                                i++;                                
                                }
                                 res.render('gag.ejs', {data:results});
                            
                        }
                    }
                        );                     
       });                     
                    
//****************************


    // Create our Express router
    var router = express.Router();

    // Create endpoint handlers for /items
    router.route('/items')
        .post(authController.isAuthenticated, itemController.postItems)
        .get(authController.isAuthenticated, itemController.getItems);

    // Create endpoint handlers for /items/:item_id
    router.route('/items/:item_id')
        .get(authController.isAuthenticated, itemController.getItem)
        .put(authController.isAuthenticated, itemController.putItem)
        .delete(authController.isAuthenticated, itemController.deleteItem);

    // Create endpoint handlers for /users
    router.route('/users')
        .post(userController.postUsers)
        .get(authController.isAuthenticated, userController.getUsers);

    // Create endpoint handlers for /clients
    router.route('/clients')
        .post(authController.isAuthenticated, clientController.postClients)
        .get(authController.isAuthenticated, clientController.getClients);

    // Create endpoint handlers for oauth2 authorize
    router.route('/oauth2/authorize')
        .get(authController.isAuthenticated, oauth2Controller.authorization)
        .post(authController.isAuthenticated, oauth2Controller.decision);

    // Create endpoint handlers for oauth2 token
    router.route('/oauth2/token')
        .post(authController.isClientAuthenticated, oauth2Controller.token);

    // Create our Express router
    var root = express.Router();

    root.get('/facebook', function(req, res) {
        res.render('facebook.ejs');
    });

    root.get('/google', function(req, res) {
        res.render('google.ejs');
    });
    root.get('/twitter', function(req, res) {
        res.render('twitter.ejs');
    });

    root.get('/login', function(req, res) {
        res.render('login.ejs');
    });

    // Register all our routes with /api
    app.use('/api', router);
    app.use('/', root);

    return app;
};